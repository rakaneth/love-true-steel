

local BLUDGEON = {
  bludgeonBase = {
    targetType = "melee",
    dmgType = "bludgeoning",
  },
  crush = {
    base = "bludgeonBase",
    name = "Crush",
    dmg = 120,
    eDmg = 140,
    delay = 10,
    autoattack = true,
    cost = {
      edr = 1,
    },
    desc = {
      flavor = "A slow, overhand blow.",
      base = "Deals #{INFO}120%# weapon damage to one target.",
      trigger = "#{INFO}Stunned:# Deals #{INFO}140%# weapon damage instead.",
    },
    action = function(self, user, target)
      if (not target) or (not target.act) then 
        debugPrint("SKILL-CRUSH", "Attempt to crush nonexistent target")
        return 
      end
      local power = target:hasEffect("stun") and self.eDmg or self.dmg
      local atkParams = {dmgType = self.dmgType, dmgMult = power}
      user:processAttack(target, "crushes", "crush", atkParams)
      return true
    end,
  },
  sap = {
    base = "bludgeonBase",
    name = "Sap",
    dmg = 10,
    eDmg = 200,
    delay = 5,
    cost = {
      edr = 1,
    },
    desc = {
      flavor = "A sharp blow meant to incapacitate rather than kill.",
      base = "Deals #{INFO}10%# weapon damage.",
      trigger = "Deals#{INFO}200%# endurance damage. Guarantees 3 turns of subdual.",
    },
    action = function(self, user, target)
      if (not target) or (not target.act) then
        debugPrint("SKILL-SAP", "Attempt to sap nonexistent target")
        return
      end
      local atkParams = {
        dmgMult = self.dmg,
        edrMult = self.eDmg,
        dmgType = self.dmgType,
      }
      user:processAttack(target, "saps", "sap", atkParams)
      if target:emptyEdr() then
        target.inactiveCounter = -30
      end
      return true
    end,
  },
}

return process(BLUDGEON)
