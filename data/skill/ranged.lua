


local RANGED = {
  rangedBase = {
    targetType = "ranged",
    dmgType = "piercing",
  },
  shoot = {
    base = "rangedBase",
    name = "Shoot",
    delay = 7,
    desc = {
      flavor = "A shot from the bow.",
      base = "Deals #{INFO}100%# weapon damage to one target."
    },
    action = function(self, user, tgt)
      if (not tgt) or (not tgt.act) then
        debugPrint("SKILL-SHOOT", "Attempt to shoot at nonexistent target")
        return
      end
      local PROJ = require 'src.projectile'
      local projParams = {
        target = {x = tgt.x, y = tgt.y},
        x = user.x,
        y = user.y,
        name = "arrow",
        map = user.map, 
        moveDelay = 1,
        payload = function(self, actor, x, y)
          if not actor then return end
          debugPrint("PAYLOAD", "target: %s", actor.eName)
          user:processAttack(actor, "shoots", "shoot", {dmgType = "piercing"})
        end,
      }
      local arrow = PROJ(projParams)
      GAME:addEntity(arrow)
      GAME:schedule(arrow)
      return true
    end,
  },
}

return process(RANGED)
