

local COMBAT = {
  coupDeGrace = {
    name = "Coup de Grace",
    targetType = "melee",
    delay = 10,
    desc = {
      flavor = "A quick, finishing blow.",
      base = "Slays a subdued target instantly.",
      trigger = "May only be used on subdued targets."
    },
    preUse = function(self, user, target)
      return not target.active
    end,
    errMsg = "Coup de Grace only works on helpless targets.",
    action = function(self, user, target)
      target:die(user)
      return true
    end,
  }
}

return process(COMBAT)
