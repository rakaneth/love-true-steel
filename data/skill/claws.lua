local CLAWS = {
  clawBase = {
    targetType = "melee",
    dmgType = "slashing",
  },
  mangle = {
    base = "clawBase",
    name = "Mangle",
    dmg = 50,
    delay = 5,
    cost = {
      edr = 1,
    },
    desc = {
      flavor = "A quick rake with the claws, meant to bleed the foe.",
      base = "Deals #{INFO}50%# weapon damage to one target and applies #{WARNING}bleeding.#",
    },
    action = function(self, user, target)
      if (not target) or (not target.alive) then
        debugPrint("SKILL-MANGLE", "Attempt to mangle nonexistent target")
        return
      end
      local atkParams = {dmgType = self.dmgType, dmgMult = self.dmg}
      local hit = user:processAttack(target, "mangles", "mangle", atkParams)
      if hit then
        target:applyEffect("bleed", {attacker = user.id})
      end
      self.cd = 10
      return true
    end,
  },
  maul = {
    base = "clawBase",
    name = "Maul",
    dmg = 110,
    eDmg = 130,
    delay = 10,
    autoattack = true,
    desc = {
      flavor = "Rip and tear savagely into one foe.",
      base = "Deals #{INFO}110%# weapon damage to one target.",
      trigger = "#{INFO}Bleeding:# Deals #{INFO}130%# weapon damage instead, extending the #{WARNING}bleed# effect.",
    },
    action = function(self, user, target)
      if (not target) or (not target.alive) then
        debugPrint("SKILL-MAUL", "Attempt to maul nonexistent target")
        return
      end
      local bleeding = target:hasEffect("bleed")
      local power = bleeding and self.eDmg or self.dmg
      local atkParams = {dmgType = self.dmgType, dmgMult = power}
      local hit = user:processAttack(target, "mauls", "maul", atkParams)
      if hit and bleeding then
        target:applyEffect("bleed", {attacker = user.id})
      end
      return true
    end,
  },
}


return process(CLAWS)
