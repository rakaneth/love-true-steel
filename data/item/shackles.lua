local MANACLES = {
  baseManacle = {
    glyph = ";",
    desc = "Manacles used for taking prisoners.",
    targetType = "melee",
    destroy = false,
    slot = '2H',
    onUse = function(self, owner, target)
      local strings = require 'data.strings'
      if target.active then
        if owner == GAME.player then 
          GAME:addMsg(strings.shackleFailMsg)
        end
        return false
      end
      owner:giveItem(self, target)
      target:equip(self)
      target:applyEffect("charmed", {attacker = owner.id})
      target:pushState("Breaking")
      local nam = self.eName
      local msg = strings.shackleUseMsg
      GAME:addMsg(msg, owner.eName, nam, target.eName)
      return true
    end,
    rarity = 100,
  },
  ironManacle = {
    color = 'grey',
    name = "iron manacles",
    base = "baseManacle",
    breakDiff = 20,
    acc = -100,
    dmg = -100,
  },
  soulShackle = {
    color = 'silver',
    name = "soul shackles",
    base = "baseManacle",
    breakDiff = 70,
    acc = -100,
    dmg = -100,
    pwr = -100,
    sav = -100,
  }
}

return process(MANACLES)
