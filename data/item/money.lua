local RNG = require 'src.rng'


local MONEY = {
   baseCoin = {
    name = "coins",
    glyph = '$',
    autopickup = true,
    amount = 0,
    onPickup = function(self, owner)
      owner.money = (owner.money or 0) + self.amount
      owner:removeItem(self)
    end,
    rarity = 30,
    finalize = function(self)
      self.amount = RNG:roll(self.range, 1)
      self.eName = self.amount .. " silver"
    end,
  },
  smallCoins = {
    base = "baseCoin",
    desc = "A few scattered silver coins,",
    rarity = 30,
    color = 'silver',
    range = '1d8',
    tier = 1,
  },
  medCoins = {
    base = "baseCoin",
    desc = "A pile of silver",
    rarity = 20,
    color = 'silver',
    range = '1d8+8',
    tier = 2,
  },
  bigCoins = {
    base = "baseCoin",
    desc = "A large pile of silver",
    rarity = 10,
    color = 'gold',
    range = '1d8+16',
    tier = 3,
  },
}

return process(MONEY)
