local HEALING = {
  baseSalve = {
    glyph = '!',
    onUse = function(self, owner, target)
      local strings = require 'data.strings'
      local useMsg = strings.salveUseMsg
      local failMsg = strings.salveFailMsg
      if owner:fullVit() then
        GAME:addMsg(failMsg, owner.eName)
        return false
      else
        GAME:addMsg(useMsg, owner.eName, self.eName)
        local eff = {
          power = self.amount,
          duration = 10
        }
        owner:applyEffect("regen", eff)
        owner:removeEffect("bleed")
        return true
      end
    end,
  },
  lesserSalve = {
    base = "baseSalve",
    name = "lesser salve",
    color = "yellow",
    amount = 0.5,
    rarity = 30,
    tier = 1,
  },
  greaterSalve = {
    base = "baseSalve",
    name = "greater salve",
    color = "orange",
    amount = 1,
    rarity = 10,
    tier = 3,
  },
}

return process(HEALING)
