
  --TODO: more monsters
local HUMANOIDS = {
  baseTroll = {
    glyph = "T",
    ai = "Charge",
    startInventory = {
      autoequip = true,
    }
  },
  trog = {
    name = "trog",
    base = "baseTroll",
    desc = "A lesser troll",
    color = "green",
    rarity = 10,
    str = 15,
    spd = 5,
    tier = 1,
    startInventory = {
      equip = {"trogHide"},
    },
  },
  troll = {
    name = "troll",
    base = "baseTroll",
    desc = "A giant carrying a tree trunk for a club,",
    color = "yellow",
    rarity = 15,
    str = 35,
    stam = 35,
    skl = 5,
    tier = 2,
    lpt = 0.1,
    ept = 0.2,
    startInventory = {
      equip = {"maul", "trollHide"},
    },
  },
  goblin = {
    name = "goblin",
    glyph = "g",
    desc = "A short, foul, wicked-looking creature",
    color = "orange",
    rarity = 20,
    ai = "Charge",
    str = 5,
    spd = 20,
    tier = 1,
    startInventory = {
      autoequip = true,
      equip = {"claws"},
    }
  },
}

return process(HUMANOIDS)
