local MEDARMOR = {
  baseMed = {
    slot = 'body',
    glyph = '[',
    color = 'brown',
    keywords = {"armor", "medium"},
  },
  leather = {
    base = "baseMed",
    name = "leather jerkin",
    dfp = 10,
    enc = 5,
    rarity = 20,
  },
  brig = {
    base = "baseMed",
    name = "brigandine mail",
    dfp = 15,
    enc = 7,
    tier = 2,
    rarity = 10,
  },
  chain = {
    base = "baseMed",
    name = "chain hauberk",
    dfp = 20,
    enc = 15,
    tier = 3,
    rarity = 10,
  },
}

return process(MEDARMOR)
