local BOWS = {
  baseBow = {
    glyph = '{',
    skills = {"shoot"},
    slot = '2H',
    keywords = {"bow", "ranged"}
  },
  shortBow = {
    base = "baseBow",
    name = "shortbow",
    rarity = 15,
    dmg = 5,
    acc = 15,
  },
}

return process(BOWS)
