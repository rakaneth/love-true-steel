local MONSTERARMOR = {
  trogHide = {
    slot = 'body',
    name = 'trog hide',
    dfp = 10,
    enc = 10
  },
  trollHide = {
    slot = 'body',
    name = 'troll hide',
    dfp = 15,
    enc = 15,
  },
  claws = {
    slot = '2H',
    name = 'claws',
    acc = 10,
    dmg = 5,
    skills = {"mangle", "maul"},
  }
}

return process(MONSTERARMOR)
