local HIARMOR = {
  baseHeavy = {
    slot = 'body',
    glyph = '[',
    color = 'silver',
    keywords = {"armor", "heavy"}
  },
  fullPlate = {
    base = "baseHeavy",
    name = 'full plate',
    dfp = 30,
    enc = 30,
    rarity = 10,
    tier = 3,
  },
}

return process(HIARMOR)
