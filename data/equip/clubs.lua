local CLUBS = {
  baseClub = {
    glyph = '-',
    slot = 'main',
    autoattack = "crush",
    keywords = {"bludgeon", "melee"}
  },
  shillelagh = {
    base = "baseClub",
    name = "shillelagh",
    color = 'yellow',
    dmg = 10,
    acc = 5,
    rarity = 50,
    skills = {"crush"},
  },
  maul = {
    base = "baseClub",
    name = "maul",
    slot = '2H',
    glyph = '\\',
    color = 'tan',
    dmg = 15,
    acc = 0,
    rarity = 20,
    skills = {"crush"},
  },
  blackjack = {
    base = "baseClub",
    name = "blackjack",
    slot = "off",
    dmg = 3,
    acc = 15,
    rarity = 10,
    color = 'green',
    skills = {"sap"},
  },
}

return process(CLUBS)    
