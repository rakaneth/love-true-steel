local SWORDS = {
  baseSword = {
    slot = 'main',
    glyph = '|',
    keywords = {"sword", "melee"}
  },
  shortSword = {
    base = "baseSword",
    name = "shortsword",
    rarity = 40,
    dmg = 5,
    acc = 10,
  }
}

return process(SWORDS)
