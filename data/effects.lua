local EFFECTS = {
  --base effects reset duration
  baseEff = {
    onMerge = function(self, eff)
      self.duration = eff.duration
    end,
  },
  --stun stacks duration
  stun = {
    duration = 10,
    desc = "stunned",
    onMerge = function(self, eff)
      self.duration = self.duration + eff.duration
    end,
    buff = {
      spd = -10,
      dmg = -5,
    }
  },
  --bleed stacks intensity and resets duration
  bleed = {
    duration = 10,
    desc = "bleeding",
    power = 0.1,
    onMerge = function(self, eff)
      self.duration = eff.duration
      self.power = self.power + eff.power
    end,
    tick = function(self, subject, ticks)
      local attacker = GAME:getEntities(self.attacker)
      subject:takeDmg(self.power * ticks, attacker)
    end,
  },
  --charmed resets duration. Last charm wins
  charmed = {
    desc = "charmed",
    onMerge = function(self, eff, subject)
      if eff.attacker ~= self.attacker then
        subject:removeEffect("charmed")
        subject:applyEffect("charmed", eff)
      else
        if eff.duration then self.duration = eff.duration end
      end
    end,
    onApply = function(self, subject)
      local attacker = GAME:getEntities(self.attacker)
      subject.oldFaction = {
        faction = subject.faction, 
        rep = subject:getRep(attacker.faction),
      }
      subject.master = self.attacker
      subject:setRep(attacker.faction, 100)
      subject.faction = attacker.faction
      subject:pushState("Follow")      
    end,
    onRemove = function(self, subject)
      local attacker = GAME:getEntities(self.attacker)
      subject:setRep(attacker.faction, subject.oldFaction.rep)
      subject.faction = subject.oldFaction.faction
      subject:popState("Follow")
      subject.oldFaction = nil
      subject.master = nil
    end,
  },
  regen = {
    duration = 5,
    desc = "regenerating",
    power = 0.2,
    positive = true,
    tick = function(self, subject, ticks)
      subject:gainLife(self.power * ticks)
    end,
  }
}

return process(EFFECTS)
