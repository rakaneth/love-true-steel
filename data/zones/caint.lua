return {
  name = "Caintigern",
  connections =  {
    {
      to = {
        zone = "Edge of Dream",
        map = 1,
        twoWay = true,
      },
    },
  }, 
  {
    dark = true,
    numMonsters = 5,
    numItems = 25,
    numEquips = 0,
  },
}
