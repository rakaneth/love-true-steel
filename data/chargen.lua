local CHARGEN = {
  --TODO: more classes/races
  baseChar = {
    name = "Player",
    money = 100,
    layer = 4,
    faction = "player",
    greedy = true,
    rep = {
      enemies = -100,
      player = 100,
    },
    startInventory = {
      items = {
        lesserSalve = 5,
      },
      autoequip = true,
    },
  },
  fighter = {
    base = "baseChar",
    desc = "Fighter",
    str = 15,
    stam = 15,
    skl = 15,
    smt = 5,
    name = "Rodrik",
    startInventory = {
      equip = {"shortSword", "chain"},
    },
  },
  rogue = {
    base = "baseChar",
    desc = "Rogue",
    money = 250, 
    skl = 15,
    spd = 15,
    sag = 5,
    lpt = 0.1,
    ept = 0.2,
    wpt = 0.01,
    name = "Erelanna",
    traitList = {"Dervish"},
    startSkills = {"coupDeGrace"},
    startInventory = {
      items = {ironManacle = 1}, 
      equip = {"shortSword", "blackjack", "leather"},
    },
  },
  wizard = {
    base = "baseChar",
    desc = "Wizard",
    str = 5,
    sag = 15,
    smt = 15,
    name = "Aloria",
  },
  keeper = {
    base = "baseChar",
    desc = "Wild-keeper",
    skl = 5,
    sag = 20,
    name = "Farkas",
    money = 0,
  },
  oathbound = {
    base = "baseChar",
    desc = "Hermit's Oath",
    spd = 5,
    sag = 20,
    name = "Barnabas",
    startInventory = {
      equip = {"shillelagh", "chain"},
    },
  },
}

return process(CHARGEN)
