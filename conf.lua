function love.conf(t)
  t.identity = "truesteel"
  t.window.title = "True Steel v0.22"
  t.console = true
  t.physics = false
end
