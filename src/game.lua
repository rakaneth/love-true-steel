--Global game state
local SCHED = require 'src.scheduler'

GAME = {
  entityList = {},
  maps = {},
  curMap = {},
  msgs = {},
  player = {},
  gameStates = {
    NORMAL = 1,
    INVENTORY = 2,
    LOOK = 3,
    TARGET = 4,
    SHOP = 5,
    DIALOG = 6,
    GAMEOVER = 7,
  },
  curState = 1,
  _id = 1,
  scheduler = SCHED:new(),
  cursor = {x=1, y=1},
  curSkill = {},
}

--Getters for map and zone
function GAME:getMap(mp)
  return self.maps[mp]
end

function GAME:getZone(zone)
  return self.zones[zone]
end

--calbak(item) should return true to break iteration
function GAME:forEachEntity(calbak)
  for _, lst in pairs(self.entityList) do
    for _, item in ipairs(lst) do
      if calbak(item) then return end
    end
  end
end

function GAME:getEntities(tagOrID)
  local result
  local argType = type(tagOrID)
  assert(argType == 'number' or argType == 'string', "Must give tag or ID")
  if argType == 'number' then
    local searchCB = function(item)
      if item.id == tagOrID then 
        result = item 
        return true
      end
    end
    self:forEachEntity(searchCB)
  else
    result = self.entityList[tagOrID]
  end
  return result
end

function GAME:entitiesWhere(calbak)
  local result = {}
  local cb = function(thing)
    if calbak(thing) then
      table.insert(result, thing)
    end
  end
  self:forEachEntity(cb)
  return result
end


--TODO: clean up these functions with table utils/rename
function GAME:addEntity(e)
  if not self.entityList[e.tag] then
    self.entityList[e.tag] = {}
  end
  table.insert(self.entityList[e.tag], e)
end

function GAME:removeEntity(e)
  local result
  local idx, tbl
  tbl = self.entityList[e.tag]
  if tbl then
    idx = table.indexOf(tbl, e)
    result = table.remove(tbl, idx)
  end
  if result and result.act then self:unschedule(result) end
  return result
end

function GAME:addMsg(msg, ...)
  if type(msg) == 'table' then
    self:addMsg(unpack(msg))
  else
    table.insert(self.msgs, msg:format(...))
  end
end

function GAME:switchScreen(screen)
  if self.curScreen then self.curScreen:exit() end
  self.curScreen:gotoState(screen)
  self.curScreen:enter()
end

function GAME:getState()
  for k, v in pairs(self.gameStates) do
    if v == self.curState then return k end
  end
end

function GAME:isState(state)
  return self:getState() == state
end

function GAME:setState(state)
  local newState = self.gameStates[state]
  assert(newState, "Attempt to change to invalid game state")
  self.curState = newState
end

function GAME:setDelay(entity, ticks)
  entity = entity or self.player
  local stdDelay = entity:getStat("dly")
  ticks = ticks or stdDelay
  entity.nextAction = ticks + self.scheduler:getTime() --might be deprecated
  entity.delay = ticks
  if entity == self.player then self:unpause() end
end

function GAME:schedule(entity)
  self.scheduler:add(entity)
end

function GAME:unschedule(entity)
  self.scheduler:remove(entity)
end

function GAME:start()
  GAMESTART = false
  self.scheduler:unlock()
end

function GAME:pause()
  self.scheduler:lock()
end

function GAME:unpause()
  self.scheduler:unlock()
end

function GAME:refreshSchedule()
  self.scheduler:clear()
  self:forEachEntity(function(e)
    if e:isPresent() and e.act then
      self:schedule(e)
    end
  end)
end

function GAME:tick()
  self.scheduler:tick()
end

function GAME:getTime()
  return self.scheduler:getTime()
end

function GAME:changeLevel(mp, x, y)
  self.curMap = mp
  self.player.map = mp.id
  self.player.x = x
  self.player.y = y
  self:refreshSchedule()
end

function GAME:setCursor(x, y)
  self.cursor.x, self.cursor.y = x, y
end

function GAME:underCursor()
  local offset = GFX:offset()
  local testX = self.cursor.x + offset.x
  local testY = self.cursor.y + offset.y
  return self.curMap:objectsAt(testX, testY)
end

function GAME:playerVisible(x, y)
  return x == self.player or self.player:isVisible(x, y)
end

return GAME
