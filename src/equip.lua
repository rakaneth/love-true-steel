local ENTITY = require 'src.entity'
local PLACE = require 'interface.place'
local class = require 'vendor.middleclass.middleclass'

local Equipment = class("Equipment", ENTITY)
Equipment:include(PLACE) 

local Equip = {
  owner = 0, --an entity ID
  dmg = 0,
  acc = 0,
  dfp = 0,
  enc = 0,
  pwr = 0,
  sav = 0,
  equipped = false,
  tier = 1,
}

function Equipment:initialize(t, block)
  block = block or {}
  assert(block.slot, "Equipment must provide a slot")
  ENTITY.initialize(self, t, block)
  
  for k, v in pairs(block) do
    if k ~= 'name' and k ~= 'color' then self[k] = v end
  end
  
  for ek, ev in pairs(Equip) do
    self[ek] = block[ek] or ev
  end
  
  self.equipped = false
  self.map = self.map or GAME.curMap.id
  self.skills = self.skills or {}
end

SAVE.registerClass(Equipment)

return Equipment
  
