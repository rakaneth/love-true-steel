--global utility functions and shims
--pretty prints a table
function table.pprint(tbl, level)
  level = level or 0
  local padding = level > 0 and string.rep(' ', level*2) or ""
  local bracePad = level > 1 and string.rep(' ', (level-1)*2) or ""
  print(bracePad .. "{")
  for k, v in pairs(tbl) do
    if type(v) == 'table' and level < 5 then
      print(padding .. k .. " =")
      table.pprint(v, level + 1)
    else
      print(padding .. ("%s = %s,"):format(k, v))
    end
  end
  print(bracePad .. "}")
end

--prints array tables in list format
function table.lprint(tbl, key)
  local vPrint
  local toPrint = {}
  for i, v in ipairs(tbl) do
    if type(v) == 'table' then
      vPrint = v[key]
    else
      vPrint = v
    end
    table.insert(toPrint, vPrint)
  end
  print(table.concat(toPrint, ","))
end

--returns true if any object in array satisfies the callback
function table.any(tbl, calbak)
  for _, v in ipairs(tbl) do
    if calbak(v) then return true end
  end
  return false
end

--returns true if all objects in array satisfy the callback
function table.all(tbl, calbak)
  for _, v in ipairs(tbl) do
    if not calbak(v) then return false end
  end
  return true
end

--return the first object in the array that satisfies the callback
--return nil if no object found
function table.first(tbl, calbak)
  for _, v in ipairs(tbl) do
    if calbak(v) then return v end
  end
  return nil
end

--return all objects in the array that satisfy the callback
--return empty table if no objects satsify the callback
function table.where(tbl, calbak)
  local result = {}
  for i, v in ipairs(tbl) do
    if calbak(v) then result[#result+1] = v end
  end
  return result
end

--returns a shallow copy of tbl
function table.copy(tbl)
  if type(tbl) ~= "table" then return t end
  local meta = getmetatable(tbl)
  local target = {}
  for k, v in pairs(tbl) do target[k] = v end
  setmetatable(target, meta)
  return target
end

--returns a deep copy of tbl
function table.clone(tbl)
  if type(tbl) ~= "table" then return t end
  local meta = getmetatable(tbl)
  local target = {}
  for k, v in pairs(tbl) do
    if type(v) == "table" then
      target[k] = table.clone(v)
    else
      target[k] = v
    end
  end
  setmetatable(target, meta)
  return target
end

--returns a weighted choice from a table in format:
--{5: 'orc', 10: 'rat', 15: 'sword'}, etc
function table.sample(tbl)
  local bowl = {}
  for i, v in pairs(tbl) do
    assert(type(i) == 'number', 'Table to be sampled in wrong format')
    for _ = 1, i do
      table.insert(bowl, v)
    end
  end
  return table.random(bowl)
end

--converts truthy values to hard true and falsey values to hard false
function tobool(v)
  return not not v
end

--debug message formatter
function debugPrint(phase, msg, ...)
  if type(phase) == 'table' then
    debugPrint(unpack(phase))
  else
    local p = ("|%d|[%s]"):format(GAME:getTime(), phase) 
    local fMsg = p .. msg:format(...)
    print(fMsg)
  end
end

--non-destructive merge, takes optional require string
function merge(mod, base)
  local b
  if type(base) == 'string' then
    b = require(base)
  else
    b = base
  end
  for k, v in pairs(b) do
    if type(mod[k] or false) == 'table' and type(v) == 'table' then
      merge(mod[k], v)
    elseif not mod[k] then 
      mod[k] = v 
    end
  end
end

--merges base tables 
function process(datatable)
  for _, v in pairs(datatable) do
    local base = v.base
    if base and datatable[base] then
      merge(v, datatable[base])
    end
  end
  return datatable
end

--puts tables in required files into one table
function compile(tbl, folder)
  local dataFolder = 'data/' .. folder
  assert(love.filesystem.exists(dataFolder), "Attempt to compile nonexistent folder")
  local files = love.filesystem.getDirectoryItems(dataFolder)
  for _, file in ipairs(files) do
    local fName = string.match(file, "(.-).lua")
    local reqName = string.format("data.%s.%s", folder, fName)
    debugPrint("COMPILE", "%s: merging %s", folder, reqName)
    merge(tbl, reqName)
  end
  return tbl
end

--turns a Dijkstra map into a flee map
function updateFleeMap(dm, entity)
  dm:compute()
  local m = dm:getMap()
  for x = 1, #m do
    for y = 1, #m[x] do
      if m[x][y] < math.huge then m[x][y] = m[x][y] * -1 end
    end
  end
  m[entity.x][entity.y] = math.huge
end

--turns a Dijkstra map into a range map
function setRange(dm, range)
  dm:compute()
  local m = dm:getMap()
  for x = 1, #m do
    for y = 1, #m[x] do
      if m[x][y] == range then dm:addGoal(x, y) end
    end
  end
  dm:compute()
end
  

