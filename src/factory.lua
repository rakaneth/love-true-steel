local MONSTERS = require 'data.monsters'
local ITEMS = require 'data.items'
local CHARGEN = require 'data.chargen'
local EQUIPMENT = require 'data.equipment'
local ACTOR = require 'src.actor'
local ITEM = require 'src.item'
local EQUIP = require 'src.equip'
local SKILLS = require 'data.skills'

local Factories = {}

function Factories.make(tag, autoplace, entityType, entityClass, optBlock, untracked)
  local eTypes = {
    monster = MONSTERS,
    player = CHARGEN,
    item = ITEMS,
    equipment = EQUIPMENT,
  }
  local block = eTypes[entityType][entityClass] or {}
  optBlock = optBlock or {}
  for k, v in pairs(optBlock) do
    block[k] = v
  end
  local ctors = {
    monster = ACTOR,
    player = ACTOR,
    item = ITEM,
    equipment = EQUIP
  }
  --construct entity
  local e = ctors[entityType](tag, block)
  
  --run finalizer
  if e.finalize then 
    debugPrint("FACTORY", "Running finalizer for %s", e.eName)
    e:finalize() 
  end
  
  --finalize starting inventory
  if e.startInventory then 
    local shouldEquip = e.startInventory.autoequip
    if e.startInventory.items then
      for ik, iv in pairs(e.startInventory.items) do
        local inv = Factories.make("start", false, "item", ik, {numUses = iv}, true)
        e:getItem(inv)
      end
    end
    if e.startInventory.equip then
      for _, ev in ipairs(e.startInventory.equip) do
        debugPrint("FACTORY", "Making %s for %s", ev, e.eName)
        local eq = Factories.make("start", false, "equipment", ev, {}, true)
        debugPrint("FACTORY", "Made %s for %s", eq.eName, e.eName)
        e:getItem(eq)
        if shouldEquip then e:equip(eq) end
        local eqDone = e:getEquipped(eq.slot)
        assert(shouldEquip and eqDone, "Error equipping " .. e.eName) 
      end
    end
  end
  
  --add skills
  if e.startSkills then
    if not e.skills then e.skills = {} end
    for _, sk in ipairs(e.startSkills) do
      debugPrint("FACTORY", "Adding skill %s to %s", sk, e.eName)
      e:addSkill(sk)
    end
  end

  --add to entity list
  if not untracked then 
    e.map = e.map or GAME.curMap.id
    GAME:addEntity(e) 
    if autoplace then e:place() end
  end

  --add to scheduler and set tempers for creatures
  if entityType == 'monster' or entityType == 'player' then
    e.curVit = e.curVit or e:getStat("vit")
    e.curEdr = e.curEdr or e:getStat("edr")
    e.curWil = e.curWil or e:getStat("wil")
  end

  --put player in player state
  if entityType == 'player' then e:gotoState("Player") end

  --put monsters in appropriate AI state
  if entityType == 'monster' and e.ai then e:gotoState(e.ai) end
  
  return e
end

function Factories.getEntityTier(eType, tier)
  local result = {}
  local eTypes = {
    monster = MONSTERS,
    equipment = EQUIPMENT,
    item = ITEMS,
  }
  local typeTbl = eTypes[eType]
  if not typeTbl then
    local errMsg = "[Factories:getEntityTier()]Invalid eType %s"
    print(errMsg:format(eType))
    return
  end
  for k, v in pairs(typeTbl) do
    if v.tier and v.tier == tier then
      result[v.rarity] = k
      debugPrint("FACTORIES.GETENTITYTIER()", "%d, %s", v.rarity, k)
    end
  end
  return result
end

function Factories.seed(eType, mp, num)
  num = num or 10
  if num < 1 then return end
  numTimes = RNG:roll(num)
  debugPrint("FACTORIES.SEED()", "Making %d %s on %s", numTimes, eType, mp.id)
  local cands = Factories.getEntityTier(eType, mp.tier)
  local eTypes = {
    monster = MONSTERS,
    equipment = EQUIPMENT,
    item = ITEMS,
  }
  local tbl = eTypes[eType]
  local candTbl
  for _ = 1, numTimes do
    cand = table.sample(cands)
    debugPrint("FACTORIES.SEED()", "cand = %s", cand)
    if cand then
      candTbl = tbl[cand] 
      Factories.make(mp.id, true, eType, cand, {map = mp.id})
    end
  end
end



return Factories
