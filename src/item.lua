local ENTITY = require 'src.entity'
local PLACE = require 'interface.place'
local class = require 'vendor.middleclass.middleclass'

local ItemBase = {
  numUses = 1,
  onUse = function(self, owner, target) 
    local msg = require('data.strings').useItemFailMsg
    GAME:addMsg(msg, self.eName)
    return false 
  end,
  onPickup = function(self, owner) return true end,
}

local Item = class("Item", ENTITY)
Item:include(PLACE)

function Item:initialize(t, block)
  block = block or {}
  ENTITY.initialize(self, t, block)
  for k, v in pairs(block) do
    if k ~= 'name' and k ~= 'color' then self[k] = v end
  end
  self.numUses = self.numUses or ItemBase.numUses
  if type(self.destroy) == "nil" then self.destroy = true end
  self.owner = 0 --an entity ID
  --between 0 (can't drop) and 100 (always drops)
  self.rarity = self.rarity or ItemBase.rarity 
  --CB is function taking owner, target params
  self.onUse = self.onUse or ItemBase.onUse 
  self.map = self.map or GAME.curMap.id
end

function Item:use(o, target)  
  if not target and self.targetType and o == GAME.player then
    GAME:setState("TARGET")
    GAME.player.curItem = self
    GAME:setCursor(GAME.player.x, GAME.player.y)
  elseif self:onUse(o, target) then
    debugPrint("ITEM", "%s uses %s on %s", o.eName, self.eName, target.eName)
    self.numUses = self.numUses - 1
    if self.numUses <= 0 and self.destroy then
      local errMsg = ("Error removing item %s from inventory"):format(self.eName)
      assert(o:removeItem(self), errMsg)
    end
    o.curItem = nil
  end
  GFX:update()
end

function Item:getOwner()
  return GAME:getEntities(self.owner)
end

SAVE.registerClass(Item)

return Item
