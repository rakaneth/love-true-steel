local ENTITY = require 'src.entity'
local class = require 'vendor.middleclass.middleclass'

local Proj = class("Projectile", ENTITY)

function Proj:initialize(block)
  local checks = block and block.target and block.target.x and block.target.y and block.map
  assert(checks, "Invalid projectile")
  if not block.layer then block.layer = 3 end
  if not block.glyph then block.glyph = '^' end
  ENTITY.initialize(self, "projectile", block)
  local m = GAME:getMap(block.map)
  local dCalbak = function(x, y)
    return not m:isBlocked(x, y)
  end
  local dMap = ROT.DijkstraMap(block.target.x, block.target.y,m.width, m.height, dCalbak)
  dMap:compute()
  local px, py = block.x, block.y
  self.path = {}
  self.map = block.map
  self.target = block.target
  self.payload = block.payload
  self.moveDelay = block.moveDelay
  self.spd = 100
  while px ~= block.target.x or py ~= block.target.y do
    local dx, dy = dMap:dirTowardsGoal(px, py)
    px = px + dx
    py = py + dy
    table.insert(self.path, {x=px, y=py})
  end
  local st = table.remove(self.path, 1)
  self.x, self.y = st.x, st.y
end

function Proj:moveAlongPath()
  local nxt = table.remove(self.path, 1)
  local m = GAME:getMap(self.map)
  local checkActor = function(x, y)
    return table.first(m:objectsAt(x, y), function(e)
      return e.alive and e.tag ~= "projectile"
    end)
  end
  local actor = checkActor(self.x, self.y)
  if not actor then 
    self.x, self.y = nxt.x, nxt.y
    actor = checkActor(self.x, self.y)
  end
  if actor or #self.path == 0 then
    local msg = {
      "PROJECTILE", "%s drops payload on %s at (%d, %d)",
      self.eName,
      actor and actor.eName or "No one",
      self.x, self.y
    }
    debugPrint(msg)
    self:payload(actor, self.x, self.y)
    return true
  end
  return false
end

function Proj:act()
  if self:moveAlongPath() then
    GAME:removeEntity(self)
  else
    GAME:setDelay(self, self.moveDelay)
  end
  GFX:update()
end

function Proj:getStat(stat)
  if stat == "dly" then return self.moveDelay end
  return 0
end

SAVE.registerClass(Proj)

return Proj
