local RNG = {
  d100 = ROT.Dice(100, 1),
  d8 = ROT.Dice(8, 1),
}

function RNG:roll(diceString, min)
  return ROT.Dice.roll(diceString, min)
end

function RNG:rollEight()
  return self.d8:roll()
end

--Returns sux/fail, by how much
function RNG:pctChance(chance)
  local r = self.d100:roll()
  return r <= chance, math.abs(chance - r)
end

function RNG:oppTest(attRating, defRating, baseDiff)
  baseDiff = baseDiff or 50
  return self:pctChance(attRating + baseDiff - defRating)
end

return RNG
