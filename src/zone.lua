local class = require 'vendor.middleclass.middleclass'

local Zone = class("Zone")
Zone.static.COUNTER = 1

function Zone:initialize(block)
  block = block or {}
  self.levels = {}
  self.connections = {}
  self.id = block.name or string.format("Zone%02d", Zone._counter)
  for i, v in ipairs(block) do
    v.name = v.name or string.format("%s-%d", self.id, i)
    local m = MAP:create(v)
    table.insert(self.levels, m.id)
    if i > 1 then
      self:connect(self.levels[i-1], m.id)
    end
  end
  if not GAME.zones then GAME.zones = {} end
  GAME.zones[self.id] = self
  Zone.COUNTER = Zone.COUNTER + 1
end

function Zone:getMap(floor)
  if type(floor) == 'number' then
    return GAME.maps[self.levels[floor]]
  else
    return GAME.maps[floor]
  end
end

--[[
block.to: destination info (required)
block.to.zone: zone id
block.to.map: map id or floor number
block.to.x: x coord
block.to.y: y coord
block.to.twoWay: true if a two-way connection should be formed
block.from: outstair info
block.from.x - x coord
block.from.y - y coord
block.from.outLevel - the floor on current zone where outstair should appear
(default 1)
--]]
function Zone:connectZone(block)
  local outLoc, outID, errMsg, outLevel, toLoc
  if not block.to then
    errMsg = "Attempt to connect zone %s with no to block"
    print((errMsg):format(self.id))
    return
  end
  local destZone = GAME.zones[block.to.zone]
  if block.from then
    local both = block.from.x and block.from.y
    outLevel = block.from.outLevel
    outLoc = both and {x=block.from.x, y=block.from.y} or self:getMap(outLevel):getEmpty()
  else
    outLoc = self:getMap(1):getEmpty()
    outLevel = 1
  end
  self:getMap(outLevel):setTile(MAP.TILES.OUTSTAIR, outLoc.x, outLoc.y)
  outID = self.levels[outLevel]
  if not self.connections[outID] then self.connections[outID] = {} end
  if not self.connections[outID]["out"] then
    self.connections[outID]["out"] = {}
  end
  if not (block.to.x and block.to.y) then
    toLoc = destZone:getMap(block.to.map):getEmpty()
  else
    toLoc = {}
    toLoc.x, toLoc.y = block.to.x, block.to.y
  end
  self.connections[outID]["out"][outLoc.x .. "," .. outLoc.y] = {
    zone = block.to.zone, 
    map = block.to.map, 
    x = toLoc.x,
    y = toLoc.y,
  }
  if block.to.twoWay then 
    destZone:connectZone{
      from = {
        outLevel = block.to.map,
        x = toLoc.x,
        y = toLoc.y,
      },
      to = {
        zone = self.id,
        map = outLevel,
        x = outLoc.x,
        y = outLoc.y
      },
    }
  end
end

function Zone:connect(upID, lowID)
  local upper = GAME.maps[upID]
  local lower = GAME.maps[lowID]
  local locUpper = upper:getEmpty()
  local locLower = lower:getEmpty()
  upper:setTile(MAP.TILES.DOWNSTAIR, locUpper.x, locUpper.y)
  lower:setTile(MAP.TILES.UPSTAIR, locLower.x, locLower.y)
  for _, conn in ipairs({upper.id, lower.id}) do
    if not self.connections[conn] then 
      self.connections[conn] = {}
    end
  end
  if not self.connections[upper.id]["down"] then
    self.connections[upper.id]["down"] = {}
  end
  if not self.connections[lower.id]["up"] then
    self.connections[lower.id]["up"] = {}
  end
  self.connections[upper.id]["down"][locUpper.x .. "," .. locUpper.y] = {
    map = lower.id,
    x = locLower.x,
    y = locLower.y
  }
  self.connections[lower.id]["up"][locLower.x .. "," .. locLower.y] = {
    map = upper.id,
    x = locUpper.x,
    y = locUpper.y
  }
end

function Zone:getCurrentConnection(direction, x, y)
  local m = GAME.curMap
  local conn = self.connections
  if conn[m.id] and conn[m.id][direction] then
    return self.connections[m.id][direction][x .. "," .. y]
  end
  return nil
end

function Zone:changeLevel(direction)
  local dest
  local m = GAME.curMap
  local x, y = GAME.player.x, GAME.player.y
  local conn = self:getCurrentConnection(direction, x, y)
  if not conn then
    local errMsg = "No %s connection in map %s"
    print(string.format(errMsg, direction, m.id))
    return
  end
  if conn.zone then GAME.curZone = GAME.zones[conn.zone] end
  dest = GAME.curZone:getMap(conn.map)

  GAME:changeLevel(dest, conn.x, conn.y)
end

SAVE.registerClass(Zone)

return Zone
