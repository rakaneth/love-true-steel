local class = require 'vendor.middleclass.middleclass'
local COLORS = require 'src.colors'

local DrawInfo = {
  glyph = '@',
  fg = COLORS:getColor('white'),
  layer = 1
}

local Entity = class("Entity")

--[[
Creates an entity.
t: tag of entity for easy location
block.name: display name of entity (default 'unknown')
block.desc: description of entity
block.glyph: display character of entity (default '@')
block.color: display color of entity
block.layer: the logical layer to draw the entity on.
Highest layer drawn last.
--]]
function Entity:initialize(t, block)
  self.id = GAME._id
  block = block or {}
  self.tag = t
  self.eName = block.name or "unknown"
  self.desc = block.desc or "No description"
  self.color = block.color and COLORS:getColor(block.color) or DrawInfo.fg
  self.eColor = block.color or 'white'
  self.glyph = block.glyph or DrawInfo.glyph
  self.layer = block.layer or DrawInfo.layer
  GAME._id = GAME._id + 1
end

function Entity:__tostring()
  return string.format("%s %s %d", self.eName, self.glyph, (self.id or 0))
end

return Entity
