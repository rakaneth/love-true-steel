local class = require 'vendor.middleclass.middleclass'
local COLORS = require 'src.colors'

--FixedMap Generator class
local FixedMap = class("FixedMap")
function FixedMap:initialize(width, height, fileName)
  self.width = width
  self.height = height
  self.fileName = fileName
end

function FixedMap:create(calbak)
  local f = string.format("maps.%s", self.fileName)
  local sTable = require(f)
  local oErrMsg = "Irregular number of rows in fixed map"
  assert(#sTable == self.height, oErrMsg)
  for y, col in ipairs(sTable) do
    for x = 1, #col do
      local iErrMsg = "Irregular length of row %d in fixed map: %d != %d"
      assert(#col == self.width, iErrMsg:format(x, self.width, #col))
      local v
      local c = col:sub(x, x)
      if c == "+" then
        v = 2
      elseif c == "#" then
        v = 1
      else
        v = 0
      end
      calbak(x, y, v)
    end
  end
end

--Tile class
local Tile = class("Tile")
function Tile:initialize(glyph, fgs, see, walk, desc)
  self.glyph = glyph
  self.fg = fgs
  self.see = see 
  self.walk = walk
  self.desc = desc
end

function Tile:__eq(other)
  local g = self.glyph == other.glyph
  local fg = self.fg == other.fg
  local see = self.see == other.see
  local walk = self.walk == other.walk
  local desc = self.desc == other.desc
  --[[local msg = {
    "TILE.__EQ",
    "g: %s, fg: %s, see: %s, walk: %s, desc: %s",
    g, fg, see, walk, desc
  }
  debugPrint(msg)]]
  return g and fg and see and walk and desc
end

--Map class
local Map = class("Map")
Map._counter = 1

function Map:initialize(width, height)
  self.width = width
  self.height = height
  self.tiles = {}
  self.defaultName = string.format("map%02d", Map._counter)
  Map._counter = Map._counter + 1
end

--Map statics
Map.static.TILES = {
  NULLTILE    = Tile:new('', nil, false, false, "No tile"),
  WOODWALL    = Tile:new('#', 'WOOD', false, false, "Wooden wall"),
  STONEWALL   = Tile:new('#', 'STONE', false, false, "Stone wall"),
  WOODFLOOR   = Tile:new('.', 'WOOD', true, true, "Wood floor"),
  STONEFLOOR  = Tile:new('.', 'STONE', true, true, "Stone floor"),
  CLOSEDOOR   = Tile:new('+', 'WOOD', false, false, "Closed door"),
  OPENDOOR    = Tile:new('\'', 'STONE', true, true, "Open door"),
  DOWNSTAIR   = Tile:new('>', 'STAIRS', true, true, "Stairs down"),
  UPSTAIR     = Tile:new('<', 'STAIRS', true, true, "Stairs up"),
  OUTSTAIR    = Tile:new('<', 'OUTSTAIRS', true, true, "Stairs out"),
}

function Map.static:create(options)
  options = options or {}
  local width = options.width or 30
  local height = options.height or 30
  local m = Map:new(width, height)
  local floors = options.floors or Map.TILES.STONEFLOOR
  local walls = options.walls or Map.TILES.STONEWALL
  local genType = options.genType or 'uniform'
  local name = options.name or m.defaultName
  local fileName = options.fileName
  local ti
  
  if genType == 'cellular' and not options.connected then 
    options.connected = true 
  end

  if genType == 'fixed' then
    assert(fileName, "Cannot create fixed map witout fileName")
  end
    
  local generators = {
    uniform   = ROT.Map.Uniform(width, height, options, RNG.rng),
    digger    = ROT.Map.Digger(width, height, options, RNG.rng),
    cellular  = ROT.Map.Cellular(width, height, options, RNG.rng),
    icey      = ROT.Map.IceyMaze(width, height, RNG.rng),
    arena     = ROT.Map.Arena(width, height, RNG.rng),
    fixed     = FixedMap(width, height, fileName)
  }

  m.id = name
  m.dark = options.dark
  m.tier = options.tier or 1

  local cb = function(x, y, v)
    if v == 1 then 
      ti = walls
    elseif v == 2 then
      ti = Map.TILES.CLOSEDOOR
    else
      ti = floors
    end
    m:setTile(ti, x, y)
  end
  local isWall = function(x, y) 
    local t = m:tile(x, y)
    return t == Map.TILES.STONEWALL or t == Map.TILES.WOODWALL 
  end 
  local d = generators[genType]
  if genType == 'cellular' then 
    d:randomize(.5) 
    for _ = 1, 4 do d:create(cb) end 
  else
    d:create(cb)
  end
  if genType == 'uniform' or genType == 'digger' then
    local rooms = d:getRooms()
    for _, r in ipairs(rooms) do
      r:addDoors(isWall)
    end
    local doors = d:getDoors()
    for _, dr in ipairs(doors) do
      m:setTile(Map.TILES.CLOSEDOOR, dr.x, dr.y)
    end
  end
  if not GAME.maps then GAME.maps = {} end
  GAME.maps[name] = m
  return m
end

--Map instance methods
function Map:isOOB(x, y)
  return x < 1 or y < 1 or x > self.width or y > self.height
end

function Map:tile(x, y)
  return self.tiles[x .."," .. y] --returns actual nil on OOB
end

function Map:setTile(t, x, y)
  self.tiles[x .. "," .. y] = t
end

function Map:adj(x, y, incWalls)
  local result = {}
  for i=x-1, x+1 do
    for j=y-1, y+1 do
      local oob = self:isOOB(i, j)
      local t = self:tile(i, j)
      local skip
      if not t then
        skip = true
      else
        skip = not (t.walk or incWalls)
      end
      local isSelf = (i == x and j == y)
      if not (oob or skip or isSelf) then table.insert(result, {x=i, y=j}) end
      --print("[Map:adj]",i, j, skip, (t and t.walk), (t and t.glyph))
    end
  end
  return result
end

function Map:getEmpty()
  local x, y, t, s, o, a
  repeat
    x = RNG:roll(self.width, 1)
    y = RNG:roll(self.height, 1)
    t = self:tile(x, y)
    s = self:onStair(x, y)
    o = self:objectsAt(x, y)
    a = t and self:adj(x, y)
    --print("[Map:getEmpty]", x,y,#o,#a)
  until (t and t.walk and (not s) and #o == 0 and #a > 0)
  return {x=x, y=y}
end

function Map:objectsAt(x, y)
  local cb = function(thing)
    return thing.map == self.id and thing.x == x and thing.y == y
  end
  return GAME:entitiesWhere(cb)
end

function Map:isBlocked(x, y)
  local things = self:objectsAt(x, y)
  if things then
    for _, v in ipairs(things) do
      if v.solid then return true end
    end
  end
  local t = self:tile(x, y)
  return (self:isOOB(x, y)) or (t and not t.walk)
end

function Map:isOpaque(x, y)
  local things = self:objectsAt(x, y)
  if #things > 0 then
    for _, v in ipairs(things) do
      if v.opaque then return true end
    end
  end
  local t = self:tile(x, y)
  return (self:isOOB(x, y)) or (t and not t.see)
end

function Map:isDoor(x, y)
  local t = self:tile(x, y)
  debugPrint("ISDOOR", "T is a %s", t.desc)
  return t == Map.TILES.CLOSEDOOR or t == Map.TILES.OPENDOOR
end

function Map:openDoor(x, y)
  if not self:isDoor(x, y) then return end
  self:setTile(Map.TILES.OPENDOOR, x, y)
end

function Map:closeDoor(x, y)
  if not self:isDoor(x, y) then return end
  self:setTile(Map.TILES.CLOSEDOOR, x, y)
end

function Map:onStair(x, y)
  local t = self:tile(x, y)
  local result
  --[[local results = {
    [Map.TILES.DOWNSTAIR] = "down",
    [Map.TILES.UPSTAIR] = "up",
    [Map.TILES.OUTSTAIR] = "out",
  }]]
  if t == Map.TILES.DOWNSTAIR then
    result = "down"
  elseif t == Map.TILES.UPSTAIR then
    result = "up"
  elseif t == Map.TILES.OUTSTAIR then
    result = "out"
  end
  return result
end


SAVE.registerClass(Tile)
SAVE.registerClass(Map)

return Map
