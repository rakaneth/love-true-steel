local COLORS = require 'src.colors'

local gfx = {
  sWidth = 50,
  sHeight = 30,
  display = ROT.TextDisplay(100, 40, 'assets/VeraMono.ttf', 16),
  hudDirty = true,
  mapDirty = true,
  msgY = 31,
}

function gfx:writeColorString(text, x, y, fg, bg)
  local pattern = "()#(%b{})(.-)#()"
  local start = 1
  local sEnd = 1
  local toWriteTable = {}
  for bIdx, c, ct, aIdx in string.gmatch(text, pattern) do
    local before = string.sub(text, start, bIdx-1)
    c = string.gsub(c, "[{}]", "")
    local nfg = COLORS[c] or COLORS:getColor(c)
    table.insert(toWriteTable, {before = before, text = ct, color = nfg})
    start = aIdx
    sEnd = aIdx
  end
  local after = string.sub(text, sEnd)
  toWriteTable.after = after
  local writeStart = 0
  for _, v in ipairs(toWriteTable) do
    local beforeStart = writeStart
    local textStart = beforeStart + #v.before 
    gfx.display:write(v.before, x+beforeStart, y, fg, bg)
    gfx.display:write(v.text, x+textStart, y, v.color, bg)
    writeStart = writeStart + #v.before + #v.text
  end
  gfx.display:write(toWriteTable.after, x+writeStart, y, fg, bg)
end

function gfx:inView(x, y)
  return not (x < 1 or y < 1 or x > self.sWidth or y > self.sHeight)
end  

function gfx:offset()
  local player = GAME.player
  local m = GAME.curMap
  local calc = function(pDim, sDim, mDim)
    local hs = math.floor(sDim / 2)
    if pDim < hs or mDim <= sDim then
      return 0
    elseif pDim >= mDim - hs then
      return mDim - sDim
    else
      return pDim - hs
    end
  end
  local xo = calc(player.x, self.sWidth, m.width)
  local yo = calc(player.y, self.sHeight, m.height)
  return {x = xo,y = yo}
end
--[[DEPRECATED
function gfx:updateFOV()  
  local player = GAME.player
  local m = GAME.curMap
  player.visible = {}
  if not self.fov then
    local fcb = function(fov, x, y) 
      return not m:isOpaque(x, y) 
    end
    self.fov = ROT.FOV.Precise:new(fcb)
  end
  local cb = function(x, y, r, v)
    table.insert(player.visible, {x=x, y=y})
  end
  self.fov:compute(player.x, player.y, player.vision, cb)
end
]]

function gfx:drawMap()
  GAME.player:updateFOV()
  local o = self:offset()
  local sw = self.sWidth
  local sh = self.sHeight
  local t, c
  local m = GAME.curMap
  self.display:clear(' ', 1, 1, self.sWidth, self.sHeight)
  for x = 1+o.x, sw + o.x do 
    for y = 1+o.y, sh + o.y do
      t = m:tile(x, y)
      if t then 
        if GAME.player:isVisible(x, y) or (not m.dark) then
          c = COLORS[t.fg] or COLORS:getColor(t.fg)
          self.display:write(t.glyph, x - o.x, y - o.y, c)
        end 
      end
    end
  end
end

function gfx:drawAdj()
  local player = GAME.player
  local o = self:offset()
  for _, v in ipairs(GAME.curMap:adj(player.x, player.y)) do
    self.display:write('X', v.x-o.x, v.y-o.y, COLORS:getColor('yellow'))
  end
end

function gfx:drawObjects()
  local o = self:offset()
  local toDraw = {}
  local m = GAME.curMap
  for _, v in pairs(GAME.entityList) do
    for _, sv in ipairs(v) do
      if sv.map == m.id then table.insert(toDraw, sv) end
    end
  end
  table.sort(toDraw, function(a, b) return a.layer < b.layer end)
  for _, d in ipairs(toDraw) do
    local drawX = d.x - o.x
    local drawY = d.y - o.y
    if self:inView(drawX, drawY) and (GAME.player:isVisible(d.x, d.y) or (not m.dark)) then
      self.display:write(d.glyph, drawX, drawY, d.color)
    end
  end
end

function gfx:drawStats()
  local player = GAME.player
  local d = function(stat, row, color)
    self:writeColorString(stat, 52, row, color)
  end
  local stat1 = ("Str%3d | Dmg%3d"):format(player.str, player:getStat("dmg"))
  local stat2 = ("Stm%3d | Tou%3d"):format(player.stam, player:getStat("tou"))
  local stat3 = ("Spd%3d | Doj%3d"):format(player.spd, player:getStat("doj"))
  local stat4 = ("Skl%3d | Atk%3d"):format(player.skl, player:getStat("atk"))
  local stat5 = ("Smt%3d | Pwr%3d"):format(player.smt, player:getStat("pwr"))
  local stat6 = ("Sag%3d | Sav%3d"):format(player.sag, player:getStat("sav"))
  local stat7 = ("Vit: #{red}%d/%d#"):format(player.curVit, player:getStat("vit"))
  local stat8 = ("Edr: #{yellow}%d/%d#"):format(player.curEdr, player:getStat("edr"))
  local stat9 = ("Wil: #{INFO}%d/%d#"):format(player.curWil, player:getStat("wil"))
  local statLocation = ("Location: #{INFO}%s#"):format(GAME.curMap.id)
  local statSilver = ("Silver #{silver}%d#"):format(player.money)

  d(player.eName, 1, COLORS.PLAYERNAME)
  d(player.desc, 2, COLORS:getColor('orange'))
  d(stat1, 3)
  d(stat2, 4)
  d(stat3, 5)
  d(stat4, 6)
  d(stat5, 7)
  d(stat6, 8)
  d(stat7, 9)
  d(stat8, 10)
  d(stat9, 11)
  d(statLocation, 12)
  d(statSilver, 13)
end

function gfx:drawMsgs()
  if #GAME.msgs == 0 then return end
  local s = #GAME.msgs
  local toWrite
  for i = self.msgY, self.display:getHeight() do
    toWrite = GAME.msgs[s-i+self.msgY]
    if toWrite then self:writeColorString(toWrite, 1, i) end
  end
end

function gfx:drawInventory()
  local player = GAME.player
  if #player.inventory == 0 then return end
  for i, v in ipairs(player.inventory) do
    local toWrite = ("%s) %s"):format(string.char(96+i), v.eName)
    if v.equipped then toWrite = toWrite .. " (e)" end
    if v.numUses then toWrite = toWrite .. string.format("(%d)", v.numUses) end
    self:writeColorString(toWrite, 1, 1+i)
  end
end

function gfx:msgClear()
  local w = self.display:getWidth()
  local h = self.display:getHeight() - self.msgY + 1
  self.display:clear(' ', 1, self.msgY, w, h)
end

function gfx:statsClear()
  local w = self.display:getWidth() - self.sWidth
  local h = self.display:getHeight()
  self.display:clear(' ', self.sWidth+1, 1, w, h)
end

function gfx:drawEquip()
  local player = GAME.player
  local a = player:getEquipped("body")
  local m = player:getEquipped("main") or player:getEquipped('2H')
  local o = player:getEquipped("off") or player:getEquipped('2H')
  local t = player:getEquipped("trinket")
  local armor = a and a.eName or "Nothing"
  local mh = m and m.eName or "Nothing"
  local oh = o and o.eName or "Nothing"
  local trink = t and t.eName or "Nothing"

  local d = function(caption, item, y)
    local toWrite = ("#{INFO}%8s:# %s"):format(caption, item)
    self:writeColorString(toWrite, 52, y)
  end

  d("Armor", armor, 28)
  d("Mainhand", mh, 26)
  d("Offhand", oh, 27)
  d("Trinket", trink, 29)
end

function gfx:drawToolTip(entity)
  local d = function(y, fmtString, ...)
    self:writeColorString(fmtString:format(...), 52, y)
  end
  if entity.getEquipped then
    local wielding
    local main = entity:getEquipped('main')
    local off = entity:getEquipped('off')
    local twoH = entity:getEquipped('2H')
    local wear = entity:getEquipped('body')
    local trink = entity:getEquipped('trinket')
    if not ( main or off or twoH ) then
      wielding = "Nothing"
    elseif ( main or off ) then
      wielding = (main and main.eName or "Nothing") .. ", " .. (off and off.eName or "Nothing")
    else
      wielding = twoH.eName
    end
    if not (wear or trink) then
      wear = "Nothing"
    else
      wear = (wear and wear.eName or "Nothing") .. ", " .. (trink and trink.eName or "Nothing")
    end
    d(33, "#{INFO}Wielding:# %s", wielding)
    d(34, "#{INFO}Wearing:# %s", wear)
  end

  d(31, "#{INFO}%s#", entity.eName)
  d(32, entity.desc)
  d(37, "#{INFO}ID:# %s", entity.id)
  d(35, "#{INFO}Target:# %s", (entity.target and entity.target.eName or "Nothing"))
  d(36, "#{INFO}Faction:# %s",(entity.faction or "None"))
end

function gfx:drawRuler()
  local c, s
  for y = 1, self.display:getHeight() do
    c = y % 10 == 0 and COLORS:getColor('red') or COLORS:getColor('green')
    s = tostring(y % 10)
    self:writeColorString(s, 51, y, c)
  end
end

function gfx:drawCurrentState()
  gfx.display:clear(' ', 1, 1, 51, 1)
  local s = GAME:getState()
  local sz = {
    LOOK = "Looking (press direction keys to move cursor, Esc to cancel)",
    TARGET = "Targeting (press direction keys to select target, Enter to confirm)",
    INVENTORY = "Inventory (press corresponding key to equip/unequip/use an item, Esc to escape)"
  }
  local toWrite = sz[s]
  if toWrite then
    self:writeColorString(toWrite, 1, 1)
  end
end

function gfx:drawCursor()
  local x, y = GAME.cursor.x, GAME.cursor.y
  self:writeColorString('X', x, y, COLORS:getColor('yellow'))
end

function gfx:update()
  self.hudDirty = true
  self.mapDirty = true
end

function gfx:drawLookTargets()
  local things = GAME:underCursor()
  local offset = self:offset()
  local locX, locY = GAME.cursor.x+offset.x, GAME.cursor.y+offset.y
  local m = GAME.curMap
  local msg
  if GAME.player:isVisible(locX, locY) or (not m.dark) then
    if #things > 0 then
      self:drawToolTip(things[1])
    else
      local t = GAME.curMap:tile(locX, locY)
      if t then 
        self:writeColorString(t.desc, 52, 31)
      end
    end
  end
end

function gfx:drawSkills()
  self:writeColorString("#{INFO}---Skills---#", 52, 30)
  local toWrite
  local curSkill = GAME.player.curSkill
  for idx, sk in ipairs(GAME.player.skillTable) do
    toWrite = string.format("%d: %s", idx, sk.name)
    local color = (curSkill and curSkill.name == sk.name) and COLORS:getColor('green')
    self:writeColorString(toWrite, 52, 30+idx, color)
  end
end

function gfx:drawEffects()
  local color
  local pos = {}
  local neg = {}
  local idx, toWrite
  for _, v in pairs(GAME.player.effects) do
    if v.positive then
      pos[#pos+1] = v
    else
      neg[#neg+1] = v
    end
  end
  for i, p in ipairs(pos) do
    toWrite = ("#{green}%s(%s)#"):format(p.desc, p.duration or "")
    self:writeColorString(toWrite, 52, 13 + i)
    idx = i
  end
  for j, n in ipairs(neg) do
    toWrite = ("#{red}%s(%s)#"):format(n.desc, n.duration or "")
    self:writeColorString(toWrite, 52, 13 + (idx or 0) + j)
  end
end

return gfx
