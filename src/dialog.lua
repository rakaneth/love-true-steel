local class = require 'vendor.middleclass.middleclass'
local Dialog = class("Dialog")

function Dialog:initialize(x, y, w, h, caption)
  assert(x+w-1 <= GFX.display:getWidth(), "Dialog failed x validation")
  assert(y+h-1 <= GFX.display:getHeight(), "Dialog failed y validation")
  self.x1, self.y1 = x, y
  self.x2, self.y2 = x+w-1, y+h-1
  self.w = w
  self.h = h
  self.caption = caption
end

function Dialog:isOOB(x, y)
  return x < 0 or y < 0 or x > self.x2 or y > self.y2
end

--Translated coordinates
function Dialog:write(text, x, y, fg, bg)
  local transX, transY = x+self.x1, y+self.y1
  assert(not self:isOOB(transX, transY), "Relative coords incorrect")
  assert(#text <= self.w-2, "Text too long")
  GFX.display:write(text, transX, transY, fg, bg)
end

function Dialog:border()
  for _, xs in ipairs({0, self.w-1}) do
    for y=0, self.h-1 do
      self:write('*', xs, y)
    end
  end
  
  for _, ys in ipairs({0, self.h-1}) do
    for x=0, self.w-1 do
      self:write('*', x, ys)
    end
  end
  if self.caption then self:write(self.caption, 1, 0) end
end
 
function Dialog:clear()
  for x=1, self.w-2 do
    for y=1, self.h-2 do
      self:write(' ', x, y)
    end
  end
end

return Dialog
