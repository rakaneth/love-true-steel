local class = require 'vendor.middleclass.middleclass'

local Sched = class("Scheduler")

function Sched:initialize()
  self.list = {}
  self.time = 1
  self.locked = true
end

function Sched:tick()
  if not self.locked then
    self.time = self.time + 1
    for _, v in ipairs(self.list) do
      v.delay = v.delay - 1
      debugPrint("SCHEDULER", "Setting delay of %s to %d", v.eName, v.delay)
    end
    local toAct = table.where(self.list, function(e) return e.delay <= 0 end)
    table.sort(toAct, function(fst, snd) return fst.spd > snd.spd end)
    for _, actor in ipairs(toAct) do
      debugPrint("SCHEDULER", "%s is acting", actor)
      actor:updateFOV()
      actor:decideAction()
      actor:act()
      if actor.delay <= 0 and actor ~= GAME.player then
        debugPrint("SCHEDULER", "Actor %s forgot to set delay, doing so now", actor.eName)
        GAME:setDelay(actor)
      end
    end
    GAME:forEachEntity(function(e) 
      if e.tick then e:tick() end
      if e.alive then
        for _, sk in ipairs(e.skillTable or {}) do
          if sk.cd and sk.cd > 0 then 
            incr(sk.cd, -1, nil, 0) 
            debugPrint("SCHEDULER", "CD of %s's %s is now %d", e.eName, sk.name, sk.cd)
          end
        end
      end
    end)
  end
end

function Sched:add(entity)
  self.list[#self.list+1] = entity
  GAME:setDelay(entity)
end

function Sched:remove(entity)
  local idx = table.indexOf(self.list, entity)
  if idx == 0 then return end
  local removed = table.remove(self.list, idx)
  debugPrint("SCHEDULER", "Removing %s from scheduler", entity.eName)
  return removed
end

function Sched:clear()
  self.list = {}
end

function Sched:reset()
  self:clear()
  self.time = 1
end

function Sched:getTime()
  return self.time
end

function Sched:lock()
  self.locked = true
end

function Sched:unlock()
  self.locked = false
end

SAVE.registerClass(Sched)
return Sched
