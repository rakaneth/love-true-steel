local colors = ROT.Color
local Colors = {}

function Colors:getColor(color) return colors.fromString(color) end

Colors.WOOD = Colors:getColor('goldenrod')
Colors.STONE = Colors:getColor('grey')
Colors.EXPLORED = Colors:getColor('blue')
Colors.PLAYERNAME = Colors:getColor('yellow')
Colors.INFO = Colors:getColor('lightblue')
Colors.WARNING = Colors:getColor('orangered')
Colors.STAIRS = Colors:getColor('yellow')
Colors.OUTSTAIRS = Colors:getColor('lightgreen')

return Colors  
