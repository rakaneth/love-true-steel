local class = require 'vendor.middleclass.middleclass'
local Stateful = require 'vendor.stateful.stateful'
local Screen = class("Screen")
Screen:include(Stateful)
local Title = Screen:addState("Title")
local Play = Screen:addState("Play")
local Inv = Screen:addState("Inventory")
local Char = Screen:addState("Character")

function Screen:initialize()
  GAME.curScreen = self
end

function Screen:enter()
  print(("Entered %s screen."):format(self.screenName))
end

function Screen:exit()
  print(("Exited %s screen."):format(self.screenName))
end

function Screen:render()
  GFX.display:write(("The %s screen has no rendering method."):format(self.screenName), 1, 1)
end

function Screen:handleInput(key)
  GFX.display:write(("The %s screen has no handleInput method."):format(self.screenName), 1, 2)
end

function Title:render()
  local newGameMsg = "[N]ew Game"
  if saveExists then 
    newGameMsg = newGameMsg .. " (will delete a current save)"
  end
  GFX.display:writeCenter("True Steel I: Chronicles", 20)
  GFX.display:writeCenter(newGameMsg, 23)
  if saveExists then
    GFX.display:writeCenter("[C]ontinue", 22)
  end
end

function Title:handleInput(sc)
  BEHOLDER.trigger(sc, "title")
end

function Play:enter()
  GFX.display:clear()
  GFX:update()
end

function Play:render()
  if GFX.mapDirty then 
    GFX:drawMap()
    GFX:drawObjects()
    if GAME:isState("LOOK") or GAME:isState("TARGET") then 
      GFX:drawCursor() 
    end
    GFX.mapDirty = false
  end
  if GFX.hudDirty then
    GFX:statsClear()
    GFX:msgClear()
    GFX:drawStats()
    GFX:drawEffects()
    GFX:drawMsgs()
    if GAME:isState("INVENTORY") then GFX:drawInventory() end
    GFX:drawEquip()
    if not (GAME:isState("LOOK") or GAME:isState("TARGET")) then
      GFX:drawSkills()
    end
    GFX:drawCurrentState()
    if GAME:isState("LOOK") or GAME:isState("TARGET") then 
      GFX:drawLookTargets() 
    end
    GFX.hudDirty = false
  end
  collectgarbage()
  collectgarbage()
end

function Play:handleInput(sc, shift, alt, ctrl)
  local state = GAME.curState
  local player = GAME.player
  local curMap = GAME.curMap
  local isNormal = state == GAME.gameStates.NORMAL
  local moves = {
    kp8 = 1,
    kp9 = 2,
    kp6 = 3,
    kp3 = 4,
    kp2 = 5,
    kp1 = 6,
    kp4 = 7,
    kp7 = 8,
  }
  local m = moves[sc]
  if m then 
    BEHOLDER.trigger(state, "move", m) 
  elseif string.match(sc, "[%.%,]") and shift and isNormal then
    BEHOLDER.trigger("stairs", curMap:onStair(player.x, player.y))
  elseif string.match(sc, "%d") then
    BEHOLDER.trigger("skill", tonumber(sc))
  else
    BEHOLDER.trigger(state, sc, shift, alt, ctrl)
  end
end

SAVE.registerClass(Screen)
return Screen
