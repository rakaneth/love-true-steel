local ENTITY = require 'src.entity'
local INVENTORY = require 'interface.inventory'
local PLACE = require 'interface.place'
local VITALS = require 'interface.vitals'
local VISION = require 'interface.vision'
local TRAITS = require 'interface.traits'
local FACTION = require 'interface.factions'
local COMBAT = require 'interface.combat'
local EFFECTS = require 'interface.effect'
local SKILLS = require 'interface.skill'
local EQUIP = require 'interface.equip'
local class = require 'vendor.middleclass.middleclass'
local STATEFUL = require 'vendor.stateful.stateful'

function standardAction(actor, dm)
  local dx, dy = dm:dirTowardsGoal(actor.x, actor.y)
  if dx and dy then
    local newX = actor.x + dx
    local newY = actor.y + dy
    actor:tryMove(newX, newY)
    GAME:setDelay(actor)
    GFX:update()
  end
end

function randomMove(actor)
  actor:moveRandom()
  GAME:setDelay(actor)
  GFX:update()
end

function generateMaps()
  player = GAME.player
  local m = GAME.curMap
  local dcb = function(x, y)
    local m = GAME.curMap
    return not m:isBlocked(x, y)
  end
  dMap = ROT.DijkstraMap:new(player.x, player.y, m.width, m.height, dcb)
  fleeMap = ROT.DijkstraMap:new(player.x, player.y, m.width, m.height, dcb)
end

local StatBlock = {
  str = 10,
  stam = 10,
  spd = 10,
  skl = 10,
  smt = 10,
  sag = 10,
  onDeath = function(self, killer) 
    --return false if creature dies
    return false 
  end, 
  onDefeat = function(self, killer)
    --return false if creature is inactive
    return false
  end,
  alive = true,
  active = true,
  vision = 5,
  lpt = 0,
  ept = 0,
  faction = "enemies",
  rep = {player = -100, enemies = 100,},
}

local Actor = class("Actor", ENTITY)
Actor:include(INVENTORY, PLACE, VITALS, VISION, TRAITS, FACTION)
Actor:include(COMBAT, EFFECTS, SKILLS, EQUIP, STATEFUL)
local Player = Actor:addState("Player")
local Charge = Actor:addState("Charge")
local Flee = Actor:addState("Flee")
local Follow = Actor:addState("Follow")
local Breaking = Actor:addState("Breaking")
local Inactive = Actor:addState("Inactive")

Actor.static.STRINGS = require 'data.strings'

function Actor:initialize(t, block)
  block = block or {layer = 3}
  if not block.layer then block.layer = 3 end
  ENTITY.initialize(self, t, block)
  for k, v in pairs(block) do
    if k ~= 'name' and k ~= 'color' then self[k] = v end
  end

  for sk, sv in pairs(StatBlock) do
    self[sk] = block[sk] or sv
  end
  self.visible = {}
  self.traitList = block.traitList or {}
  self.effects = block.effects or {}
  self.inventory = block.inventory or {limit = 12}
  self.opaque = block.opaque or false
  self.solid = block.solid or false
  self.skills = block.skills or {}
end

function Actor:die(killer)
  self.alive = self:onDeath(killer)
  if not self.alive then
    BEHOLDER.trigger("death", self, killer)
  end
end

function Actor:defeat(killer)
  self.active = self:onDefeat(killer)
  if not self.active then
    BEHOLDER.trigger("defeat", self, killer)
  end
end

function Actor:decideAction()
  if self.ai then 
    self:pushState(self.ai)
    self:decideAction()
  end
end

function Charge:decideAction()
  if not dMap then generateMaps() end
  local enemies = self:enemiesVisible()
  if (self.curVit / self:getStat("vit")) < 0.33 then
    self:pushState("Flee")
    self:decideAction()
  elseif #enemies > 0 then
    self.enemySpotted = true
    dMap:removeGoals()
    for _, enemy in ipairs(enemies) do
      dMap:addGoal(enemy.x, enemy.y)
    end
    dMap:compute()
  else
    self.enemySpotted = false
  end
end

function Charge:act()
  if self.enemySpotted then 
    standardAction(self, dMap) 
  else
    randomMove(self)
  end
end

function Flee:decideAction()
  local enemies = self:enemiesVisible()
  local healthOK = (self.curVit / self:getStat("vit")) >= 0.33
  if not fleeMap then generateMaps() end
  if healthOK and self.ai ~= "Flee" then
    self:popState("Flee")
    self:decideAction()
  elseif #enemies > 0 then
    fleeMap:removeGoals()
    for _, enemy in ipairs(enemies) do
      fleeMap:addGoal(enemy.x, enemy.y)
    end
    self.enemySpotted = true
    updateFleeMap(fleeMap, self)
  else
    self.enemySpotted = false
  end
end

function Flee:act()
  if self.enemySpotted then 
    standardAction(self, fleeMap) 
  else
    randomMove(self)
  end
end

function Follow:decideAction()
  if not dMaps then generateMaps() end
  if not self.master then 
    debugPrint("AI-FOLLOW", "%s has no master to follow", self.eName)
    self:popState("Follow")
    self:decideAction()
  else
    local master = GAME:getEntities(self.master)
    dMap:removeGoals(master.x, master.y)
    dMap:compute()
  end
end

function Follow:act()
  standardAction(self, dMap)
end

function Breaking:decideAction()
  local shackles = self:getEquipped('2H')
  if not shackles or not shackles.breakDiff then
    debugPrint("AI-BREAKING", "%s has no shackles to break", self.eName)
    self:popState("Breaking")
    self:decideAction()
  elseif RNG:oppTest(self.str, shackles.breakDiff, 0) then
    debugPrint("AI-BREAKING", "%s attempts to break %s", self.eName, shackles.eName)
    local msg = {Actor.STRINGS.actorBreakFreeMsg, self.eName, shackles.eName}
    GAME:addMsg(msg)
    shackles.equipped = false
    self:removeItem(shackles)
    self:popState("Breaking")
    if self:hasEffect("charmed") then self:removeEffect("charmed") end
  else
    self:popState("Breaking")
    self:decideAction()
    self:pushState("Breaking")
  end
end

function Inactive:decideAction()
  if (self.inactiveCounter or 0) >= (100 - self.stam) then self:gainEdr(1) end
  if self.curEdr >= 1 then
    GAME:addMsg(Actor.STRINGS.actorInactiveRecoveryMsg, self.eName)
    self:popState("Inactive")
    self.inactiveCounter = nil
    self:decideAction()
  end
end

function Inactive:act()
  GAME:setDelay(self, 10)
  if not self.inactiveCounter then
    self.inactiveCounter = 10
  else
    self.inactiveCounter = self.inactiveCounter + 10
  end
end

function Player:enteredState()
  generateMaps()
end

function Player:act()
  GAME:pause()
end


SAVE.registerClass(Actor)
return Actor
