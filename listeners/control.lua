local states = GAME.gameStates
local saveExists = love.filesystem.exists(saveFile)

local cursorMove = function(direction)
  local dx, dy = unpack(ROT.DIRS.EIGHT[direction])
  local newX = GAME.cursor.x + dx
  local newY = GAME.cursor.y + dy
  if GFX:inView(newX, newY) then
    GAME:setCursor(newX, newY)
  end
  GFX:update()
end

BEHOLDER.observe("c", "title", function()
  if saveExists then
    assert(loadGame(), "Error loading saved game")
    GAME:switchScreen("Play")
  end
end)

BEHOLDER.observe("n", "title", function()
  deleteGame()
  newGame()
  GAME:switchScreen("Play")
end)

BEHOLDER.observe(states.NORMAL, "move", function(direction)
  local player = GAME.player
  local dx, dy = unpack(ROT.DIRS.EIGHT[direction])
  local newX = player.x + dx
  local newY = player.y + dy
  local moved = player:tryMove(newX, newY)
  if moved then
    GAME:setDelay()
  end
  GFX:update()
end)

BEHOLDER.observe(states.LOOK, "move", cursorMove)

BEHOLDER.observe(states.TARGET, "move", cursorMove)

BEHOLDER.observe("inventory", function(button)
  if button == "escape" then
    GAME:setState("NORMAL")
    GFX:update()
    return
  end
  local player = GAME.player
  local idx = string.byte(button) - 96
  local item = player.inventory[idx]
  if item then
    if item.slot and not item.use then 
      if item.equipped then player:unequip(item) else player:equip(item) end
    else
      item:use(GAME.player)
    end
    GAME:setDelay()
    GAME:setState("NORMAL")
  end
  GFX:update()
end)

BEHOLDER.observe("stairs", function(stair)
  local strings = require 'data.strings'
  if not stair then
    GAME:addMsg(strings.noStairsMsg)
    GFX.hudDirty = true
  else
    GAME.curZone:changeLevel(stair)
    GAME:setDelay()
    GFX:update()
  end
end)

BEHOLDER.observe(states.NORMAL, "g", true, function()
  local player = GAME.player
  local stuff = player:here()
  print(#stuff)
  if stuff then
    for _, v in pairs(stuff) do
      player:tryPickup(v)
    end
    GAME:setDelay()
  end
end)

BEHOLDER.observe(states.NORMAL, "i", function()
  GAME:setState("INVENTORY")
  GFX:update()
end)

BEHOLDER.observe(states.NORMAL, "l", true, function()
  GAME:setState("LOOK")
  GAME:setCursor(GAME.player.x, GAME.player.y)
  GFX:update()
end)


--test button
BEHOLDER.observe(states.NORMAL, "t", function()
  GAME.player:applyEffect("stun")
  GAME.player:applyEffect("regen")
end)

BEHOLDER.observe(states.NORMAL, "s", true, function()
  local strings = require 'data.strings'
  GAME:addMsg(strings.saveMsg)
  saveGame()
  GAME:addMsg(strings.doneSaveMsg)
  GFX.hudDirty = true
end)

BEHOLDER.observe(states.NORMAL, "q", true, function()
  love.event.quit()
end)

BEHOLDER.observe("skill", function(num)
  local player = GAME.player
  local skills = player.skillTable
  local sk = skills[num]
  if sk then
    debugPrint("UI", "Selected skill %s", sk.name)
    if sk.targetType == "ranged" then
      player:useSkill(sk)
    else
      GAME.player.curSkill = sk
    end
  end
  GFX.hudDirty = true
end)

BEHOLDER.observe(states.INVENTORY, function(button)
  BEHOLDER.trigger("inventory", button)
end)

BEHOLDER.observe(states.LOOK, "escape", function()
  GAME:setState("NORMAL")
  GFX:update()
end)

BEHOLDER.observe(states.TARGET,"return", function()
  local t
  local under = GAME:underCursor()
  local actor = table.first(under, function(e) return e.act end)
  local c = ("(%d, %d)"):format(GAME.cursor.x, GAME.cursor.y)
  if actor then
    t = actor
  else
    t = GAME.cursor
  end
  local msg = {"TARGET", "Target set: %s", t.act and t.eName or c}
  local sk = GAME.player.curSkill
  local it = GAME.player.curItem
  if sk then
    GAME.player:useSkill(sk, t)
  elseif it then
    it:use(GAME.player, t)
  end

  debugPrint(msg)
  GAME:setState("NORMAL")
  GFX:update()  
end)

BEHOLDER.observe(states.TARGET, "escape", function()
  local msg = require('data.strings').targetCancelMsg
  if GAME.player.curSkill then GAME.player.curSkill = nil end
  GAME:setState("NORMAL")
  GAME:addMsg(msg)
  GFX:update()
end)

BEHOLDER.observe(states.GAMEOVER, function()
  love.event.quit()
end)
