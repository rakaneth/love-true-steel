local FACTORY = require 'src.factory'

--TODO: corpses
local function dropItems(entity)
  if entity.inventory then
    for _, item in ipairs(entity.inventory) do
      if item.rarity and RNG:pctChance(item.rarity) then
        entity:drop(item, true)
      end
    end
  end
end

BEHOLDER.observe("death", function(entity, killer)
  local killMsg
  local strings = require 'data.strings'
  if killer then
    killMsg = {strings.slainKillerMsg, killer.eName, entity.eName}
  else
    killMsg = {strings.slainNoKillerMsg, entity.eName} 
  end
  GAME:addMsg(killMsg)
  if killer and killer.target then killer.target = nil end
  dropItems(entity)
  if entity == GAME.player then 
    BEHOLDER.trigger("gameover")
  else
    GAME:removeEntity(entity)
  end
end)

--TODO: defeated mechanics
BEHOLDER.observe("defeat", function(entity, killer)
  local defeatMsg
  local strings = require 'data.strings'
  if killer then
    defeatMsg = {strings.subdueKillerMsg, killer.eName, entity.eName}
  else
    defeatMsg = {strings.subdueNoKillerMsg, entity.eName}    
  end
  dropItems(entity)
  GAME:addMsg(defeatMsg)
  entity:pushState("Inactive")
  if killer.target then killer.target = nil end
end)

BEHOLDER.observe("gameover", function()
  local strings = require 'data.strings'
  GAME:pause()
  GAME:addMsg(strings.gameOverMsg, GAME.player.eName)
  GAME:addMsg(strings.pressAnyKeyExitMsg)
  GAME:setState("GAMEOVER")
end)
