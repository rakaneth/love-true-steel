local iEquip = {}

function iEquip:unequip(item)
  local strings = require 'data.strings'
  if not self:hasItem(item) then return end
  if not item.slot then return end
  item.equipped = false
  if (not GAMESTART) and GAME:playerVisible(self) then
    GAME:addMsg(strings.unequipMsg, self.eName, item.eName)
  end
  debugPrint("UNEQUIP", "%s unequips %s", self.eName, item.eName)
  GFX.hudDirty = true
end

function iEquip:equip(item)
  local errMsg
  local strings = require 'data.strings'
  if not self:hasItem(item) then 
    errMsg = "Attempt by %s to equip unowned item %s"
    debugPrint("EQUIP", errMsg, self.eName, item.eName)
    return 
  end
  if not item.slot then 
    errMsg = "Attempt by %s to equip non-equipment %s"
    debugPrint("EQUIP", errMsg, self.eName, item.eName)
    return 
  end
  local eq = self:getEquipped(item.slot)
  if eq then self:unequip(eq) end
  if item.slot == '2H' then
    local main = self:getEquipped('main')
    local off = self:getEquipped('off')
    local prev2H = self:getEquipped('2H')
    for _, prev in ipairs({main, off, prev2H}) do
      if prev then self:unequip(prev) end
    end
  end
  local desc 
  if (item.slot == 'body' or item.slot == 'trinket') then 
    desc = 'wears'
  else
    desc = 'wields'
  end
  if (not GAMESTART) and GAME:playerVisible(self) then
    GAME:addMsg(strings.equipMsg, self.eName, desc, item.eName)
  end
  debugPrint("EQUIP", "%s equips %s", self.eName, item.eName)
  item.equipped = true
  GFX.hudDirty = true
end

function iEquip:getEquipped(slot)
    for _, v in ipairs(self.inventory) do
    if v.equipped and v.slot == slot then return v end
  end
end

function iEquip:getTotalEquipped(stat)
  local acc = 0
  for _, v in ipairs(self.inventory) do
    if v.equipped and v[stat] or false then
      acc = acc + v[stat] or 0
    end
  end
  return acc
end

--Secondary stat calcs
function iEquip:calcStat(base, secondary, penalty, adHoc)
  local s = self:getTotalEquipped(secondary)
  local p = self:getTotalEquipped(penalty)
  return base + s - p + (adHoc or 0)
end

function iEquip:getStat(stat)
  local dualWield = self:getEquipped('off')
  local derv = self:hasTrait("Dervish")
  local forms = {
    atk = {self.skl, "acc", "enc", ((dualWield and (not derv)) and -30)},
    dmg = {math.floor(self.str/10), "dmg"},
    doj = {self.spd, "luk", "enc"},
    tou = {self.stam, "dfp"},
    pwr = {math.floor((self.smt+self.sag) / 2), "pwr"},
    sav = {math.floor((self.spd+self.sag) / 2), "sav"},
    vit = {self.stam, "vit"},
    edr = {math.floor(self.stam*2.5), "edr"},
    wil = {math.floor((self.smt+self.sag)*1.3), "wil"},
    dly = {math.max(10 - math.floor(self.spd/10), 1)}
  }
  return self:calcStat(unpack(forms[stat])) + (self[stat] or 0)
end

return iEquip
