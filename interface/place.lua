--requirements:
--self.x
--self.y
--self.map
local iPlace = {}

function iPlace:place() 
  local m = GAME.maps[self.map]
  assert(m, "Error finding map to place entity")
  local loc = m:getEmpty()
  self.x, self.y = loc.x, loc.y
end

--call with an entity to return true if entity is in same space as caller
--call with no args to get a list of all entities that are in the same space as caller
function iPlace:here(target)
  local isHere = target and target.map == self.map and target.x == self.x and target.y == self.y
  if target then
    return isHere
  else
    local result = {}
    local m = self.map
    local cbX = self.x
    local cbY = self.y
    local id = self.id
    local hereCB = function(item)
      if item.id ~= id and item.map == m and item.x == cbX and item.y == cbY then table.insert(result, item) end
    end
    GAME:forEachEntity(hereCB)
    return result
  end
end

function iPlace:tryMove(x, y)
  local m = GAME.curMap
  local oob = GAME.curMap:isOOB(x, y)
  local wall = not oob and m:isBlocked(x, y) --TODO: route through Map class
  local door = not oob and m:isDoor(x, y)
  local things = m:objectsAt(x, y)
  local actor = table.first(things, function(t)
    return tobool(t.act)
  end)
  local strings = require 'data.strings'
  debugPrint("MOVEMENT", "%s tries to move to (%d, %d)", self.eName, x, y)
  if not (wall or oob or actor) then
    self.x, self.y = x, y
    local things = self:here()
    for _, item in ipairs(things) do
      if item.autopickup and self.greedy then 
        self:tryPickup(item)
      end
    end
    debugPrint("MOVEMENT", "%s moved successfully", self.eName)
    return true
  elseif actor then
    if self:isEnemy(actor) then
      local sk = self:getAutoAttack()
      local curSK = self.curSkill
      local curIT = self.curItem
      if curSK and curSK.targetType == "melee" then
        self:useSkill(curSK, actor)
      elseif curIT and curIT.targetType == "melee" then
        curIT:use(self, actor)
      elseif sk then 
        self:useSkill(sk, actor)
      else
        self:processAttack(actor, "strikes", "strike")
      end
    else
      if GAME.player:isVisible(x, y) then
        GAME:addMsg(strings.allyMoveMsg, self.eName)
      end
    end
    return true
  elseif door then
    GAME.curMap:openDoor(x, y)
    debugPrint("MOVEMENT", "%s opened door at (%d, %d)", self.eName, x, y)
    return true
  end
  debugPrint("MOVEMENT", "%s failed to move", self.eName)
  return false
end

function iPlace:moveDirection(direction)
  local dx, dy = unpack(ROT.DIRS.EIGHT[direction])
  newX = self.x + dx
  newY = self.y + dy
  return self:tryMove(newX, newY)
end

function iPlace:moveRandom()
  local r = RNG:rollEight()
  return self:moveDirection(r)
end

function iPlace:inMelee()
  local result = {}
  local m = GAME.curMap
  local adj = m:adj(self.x, self.y)
  for _, pt in ipairs(adj) do
    local things = m:objectsAt(pt.x, pt.y)
    for _, thing in ipairs(things) do
      table.insert(result, thing)
    end
  end
  return result
end

--if x and y are given, returns true if entity is at (x, y) and on curMap.
--with no args, returns true if entity is on curMap
function iPlace:isPresent(x, y)
  local onMap = self.map == GAME.curMap.id
  if x and y then
    return onMap and self.x == x and self.y == y
  end
  return onMap
end

return iPlace
