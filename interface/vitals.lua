local iVitals = {}


function iVitals:takeDmg(dmg, attacker)
  self.curVit = math.max(0, self.curVit - dmg)
  if self:emptyVit() then self:die(attacker) end
  debugPrint("VIT", "%s loses %s vitality", self.eName, dmg)
end

function iVitals:takeEdr(dmg, attacker)
  self.curEdr = math.max(0, self.curEdr - dmg)
  if self:emptyEdr() then self:defeat(attacker) end
  debugPrint("EDR", "%s loses %s endurance", self.eName, dmg)
end

function iVitals:takeWil(dmg)
  self.curWil = math.max(0, self.curWil - dmg)
  debugPrint("WIL", "%s loses %s will", self.eName, dmg)
end

function iVitals:gainLife(amt)
  self.curVit = math.min(self:getStat("vit"), self.curVit + amt)
  debugPrint("VIT", "%s gains %s vitality", self.eName, amt)
end

function iVitals:gainEdr(amt)
  self.curEdr = math.min(self:getStat("edr"), self.curEdr + amt)
  debugPrint("EDR", "%s gains %s endurance", self.eName, amt)
end

function iVitals:gainWil(amt)
  self.curWil = math.min(self:getStat("wil"), self.curWil + amt)
  debugPrint("EDR", "%s gains %s will", self.eName, amt)
end

function iVitals:restoreVit()
  self.curVit = self:getStat("vit")
  debugPrint("VIT", "%s full vitality restore", self.eName)
end

function iVitals:restoreEdr()
  self.curEdr = self:getStat("edr")
  debugPrint("EDR", "%s full endurance restore", self.eName)
end

function iVitals:restoreWil()
  self.curWil = self:getStat("wil")
  debugPrint("WIL", "%s full will restore", self.eName)
end

function iVitals:restore()
  self:restoreVit()
  self:restoreEdr()
  self:restoreWil()
end

function iVitals:fullVit()
  return self.curVit == self:getStat("vit")
end

function iVitals:fullEdr()
  return self.curEdr == self:getStat("edr")
end

function iVitals:fullWil()
  return self.curWil == self:getStat("wil")
end

function iVitals:emptyVit()
  return self.curVit == 0
end

function iVitals:emptyEdr()
  return self.curEdr == 0
end

function iVitals:emptyWil()
  return self.curWil == 0
end

return iVitals  
