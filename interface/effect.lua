--[[
Requirements:
self.effect
--]]
local EFFECTS = require 'data.effects'
local iEffects = {}

function iEffects:hasEffect(effName)
  return self.effects[effName]
end

function iEffects:getEffect(effName)
  return EFFECTS[effName]
end

--[[
effName: logical name of effect (should match one in list or be unique)
block.duration - duration of the effect in ticks
block.desc - display name of the effect
block.power - numerical power of the effect
block.attacker - ID of entity that applied the effect
block.positive - true if this effect is a positive effect
block.buff{} - Temporary numerical stat change info.
block.onMerge(self, eff) - function called when this effect stacks on itself
block.tick(self, subject, ticks) - function called when this effect ticks
block.onApply(self, subject) - function called when this effect is applied
block.onRemove(self, subject) - function called when this effect is removed
--]]
function iEffects:applyEffect(effName, block)
  local eff
  local strings = require 'data.strings'
  if self:getEffect(effName) then
    eff = table.copy(self:getEffect(effName))
  else
    eff = {}
  end
  block = block or {}
  for k, v in pairs(block) do eff[k] = v end
  if eff.buff then 
    for bk, bv in pairs(eff.buff) do
      if self[bk] then
        self[bk] = self[bk] + bv
      else
        self[bk] = bv
      end
    end
  end
  local prev = self:hasEffect(effName)
  if prev then
    prev:onMerge(eff, self)
  else
    self.effects[effName] = table.copy(eff)
    --self.effects[effName].owner = self.id
  end
  if eff.onApply then eff:onApply(self, block.attacker) end
  GAME:addMsg(strings.applyEffMsg, self.eName, eff.desc)
  debugPrint("EFFECT", "%s-%s-%d", self.eName, effName, eff.duration or -1)
  GFX.hudDirty = true
end

function iEffects:removeEffect(effName)
  local eff = self.effects[effName]
  local strings = require 'data.strings'
  if eff then 
    if eff.onRemove then eff:onRemove(self) end
    if eff.buff then
      for k, v in pairs(eff.buff) do
        if self[k] then self[k] = self[k] - v end
      end
    end
    self.effects[effName] = nil 
    GAME:addMsg(strings.removeEffMsg, self.eName, eff.desc)
  end
end
    
function iEffects:tick(ticks)
  ticks = ticks or 1
  for k, v in pairs(self.effects) do
    local maxDur = math.min(v.duration or 0, ticks)
    if v.tick then v:tick(self, maxDur) end
    if v.duration then 
      v.duration = v.duration - ticks
      if v.duration <= 0 then self:removeEffect(k) end
    end
    debugPrint("EFFECT", "%s suffers %s for %s ticks", self.eName, k, maxDur)
  end
  if self.lpt then self:gainLife(self.lpt * ticks) end
  if self.ept then self:gainEdr(self.ept * ticks) end
  if self.wpt then self:gainWil(self.wpt * ticks) end
end

return iEffects
