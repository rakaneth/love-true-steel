--interface requirements:
--self.rep,
--self.faction,
--vision interface,
--placement interface
local iFaction = {}
function iFaction:getRep(faction)
  return self.rep[faction]
end

function iFaction:changeRep(faction, amt)
  if not self.rep[faction] then
    self.rep[faction] = amt
  else
    self.rep[faction] = self.rep[faction] + amt
  end
end

function iFaction:setRep(faction, amt)
  self.rep[faction] = amt
end

function iFaction:isEnemy(entity)
  return entity.faction and self:getRep(entity.faction) < 0
end

function iFaction:isAlly(entity)
  return entity.faction and self:getRep(entity.faction) > 0
end

function iFaction:enemiesVisible()
  if not self.vision then
    local errMsg = "%s has no vision to see enemies"
    debugPrint("FACTION", errMsg, self.eName)
    return
  end
  local result = table.where(self:visibleThings(), function(e)
    return self:isEnemy(e)
  end)
  return result
end

function iFaction:enemiesInMelee()
  local result = table.where(self:inMelee(), function(e)
    return self:isEnemy(e)
  end)
  return result
end

return iFaction
