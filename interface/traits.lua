local iTraits = {}

function iTraits:hasTrait(trait)
  for _, t in ipairs(self.traitList) do
    if t == trait then
      return true
    end
  end
  return false
end

function iTraits:addTrait(trait)
  table.insert(self.traitList, trait)
end

function iTraits:removeTrait(trait)
  local errMsg, idx
  if not self:hasTrait(trait) then 
    errMsg = "[TRAIT]Attempt to remove nonexistent trait %s from %s" 
    print(errMsg:format(trait, self.eName))
    return 
  end
  idx = table.indexOf(self.traitList, trait)
  table.remove(self.traitList, idx)
end

return iTraits
