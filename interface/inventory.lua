local iInventory = {}

function iInventory:hasItem(item)
  for _, v in ipairs(self.inventory) do
    if v.id == item.id then return true end
  end
  return false
end

function iInventory:getItem(item)
  item.map = "none"
  item.x, item.y = 0, 0
  table.insert(self.inventory, item)
  GAME:removeEntity(item)
  item.owner = self.id
end

function iInventory:removeItem(item)
  if not self:hasItem(item) then return end
  local idx = table.indexOf(self.inventory, item)
  --[[DEPRECATED
  for i, v in ipairs(self.inventory) do
    if v.id == item.id then idx = i end
  end]]
  local done = table.remove(self.inventory, idx)
  return done
end

function iInventory:giveItem(item, target)
  if not self:hasItem(item) then return end
  target:getItem(item)
  self:removeItem(item)
end

function iInventory:tryPickup(item)
  local strings = require 'data.strings'
  if item.act then
    debugPrint("TRYPICKUP", "%s tried to pick up actor %s", self.eName, item.eName)
    return
  end
  local msg
  local limit = self.inventory.limit and #self.inventory < self.inventory.limit
  if limit or item.autopickup then
    self:getItem(item)
    if item.onPickup then item:onPickup(self) end
    msg = {strings.itemPickupMsg, self.eName, item.eName}
  else
    msg = {itemPickupFailMsg, self.eName, item.eName}
  end
  GAME:addMsg(msg)
  GFX:update()
end

function iInventory:drop(item, randSpot)
  item.tag = self.map
  item.map = self.map
  if randSpot then
    local m = GAME:getMap(self.map)
    local adj = m:adj(self.x, self.y)
    if #adj > 0 then
      local loc = table.random(adj)
      item.x, item.y = loc.x, loc.y
    else
      item.x, item.y = self.x, self.y
    end
  else
    item.x, item.y = self.x, self.y
  end
  if item.slot then item.equipped = false end
  GAME:addEntity(item)
  self:removeItem(item)
end

return iInventory
