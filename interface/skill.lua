local SKILLS = require 'data.skills'
local iSkill = {}

function iSkill:getWeaponSkills()
  if not self.inventory then return end
  local result = {}
  for _, v in ipairs(self.inventory) do
    if v.equipped and v.skills then
      for _, s in ipairs(v.skills) do
        sk = table.copy(SKILLS[s])
        debugPrint("GETWEAPONSKILLS", "adding %s to list", sk.name)
        table.insert(result, sk)
      end
    end
  end
  return result
end

function iSkill:addSkill(skName)
  local sk = SKILLS[skName]
  if not SKILLS[skName] then
    debugPrint("SKILL", "Attempt to add nonexistent skill %s", skName)
    return
  end
  if self:hasSkill(skName) then
    debugPrint("SKILL", "Attempt to add skill %s already known", skName)
    return
  end
  if not self.skills then self.skills = {} end
  self.skills[#self.skills+1] = table.copy(SKILLS[skName])
  self:refreshSkillTable()
end

function iSkill:removeSkill(skName)
  if not self:hasSkill(skName) then
    local msg = {
      "SKILL", "Attempt to remove unknown skill %s from %s",
      skName, self.eName
    }
    debugPrint(msg)
    return
  end
  local sk = self:getSkill(skName)
  local idx = table.indexOf(self.skills, sk)
  if idx == 0 then
    debugPrint("SKILL", "Error attempting to remove skill %s", skName)
    return
  end
  local removed = table.remove(self.skills, idx)
  self:refreshSkillTable()
  return removed
end

function iSkill:getSkill(skName)
  return table.first(self.skillTable, function(s) return s.name == skName end)
end

function iSkill:getAllSkills()
  local weap = self:getWeaponSkills()
  for _, v in ipairs(self.skills) do
    table.insert(weap, v)
  end
  return weap
end

function iSkill:canUseSkill(skill, target)
  local preUse = (not skill.preUse) or skill:preUse(self, target)
  return preUse and self:canPayCost(skill) and not self:isOnCD(skill)
end

function iSkill:isOnCD(skill)
  return skill.cd and skill.cd > 0
end

function iSkill:canPayCost(skill)
  local edrCost = skill.cost and skill.cost.edr
  local wilCost = skill.cost and skill.cost.wil
  local vitCost = skill.cost and skill.cost.vit
  local canPayEdr = (not edrCost) or self.curEdr > edrCost
  local canPayWil = (not wilCost) or self.curWil >= wilCost
  local canPayVit = (not vitCost) or self.curVit > vitCost
  return canPayEdr and canPayWil and canPayVit
end

function iSkill:hasSkill(skName)
  if not self.skillTable then self:refreshSkillTable() end
  return table.any(self.skillTable, function(s) return s.name == skName end)
end

function iSkill:payCosts(skill)
  local edrCost = skill.cost and skill.cost.edr
  local wilCost = skill.cost and skill.cost.wil
  local vitCost = skill.cost and skill.cost.vit
  if edrCost then self:takeEdr(edrCost) end
  if wilCost then self:takeWil(wilCost) end
  if vitCost then self:takeDmg(vitCost) end
end

function iSkill:getAutoAttack()
  if not self.skillTable then self:refreshSkillTable() end
  local res = table.first(self.skillTable, function(sk) return sk.autoattack end)
  if res then
    debugPrint("GETAUTOATTACK", "Current auto of %s is %s", self.eName, res.name)
  end
  return res
end

function iSkill:useSkill(skill, target)
  if not self:canUseSkill(skill, target) then
    debugPrint("SKILL", "Failed attempt by %s to use %s", self.eName, skill.name)
    if skill.errMsg and self == GAME.player then GAME:addMsg(skill.errMsg) end
    return 
  end
  if self == GAME.player and (not target) then 
    GAME.player.curSkill = skill
    GAME:setState("TARGET")
    GAME:setCursor(self.x, self.y)
  elseif skill:action(self, target) then
    GAME:setDelay(self, skill.delay)
    self:payCosts(skill)
    GAME.player.curSkill = nil
  end
  GFX:update()
end

function iSkill:refreshSkillTable()
  debugPrint("SKILL", "Refreshing skill table of %s", self.eName)
  if not self.skillTable then self.skillTable = {} end
  self.skillTable = self:getAllSkills()
end

return iSkill
