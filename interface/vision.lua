--[[
interface requirements: 
self.visible,
self.vision,
placement interface
--]]
local iVision = {}
local function fcb(fov, x, y) 
  local m = GAME.curMap
  return not m:isOpaque(x, y) 
end
local fov = ROT.FOV.Precise:new(fcb)

function iVision:updateFOV()
  self.visible = {}
  local cb = function(x, y, r, v)
    self.visible[#self.visible+1] = {x=x, y=y}
  end
  fov:compute(self.x, self.y, self.vision, cb)
end

--returns whether an entity is visible if given as one argument
--returns whether the position (x, y) is visible if given x and y coords
function iVision:isVisible(x, y)
  if x and y then
    for _, v in ipairs(self.visible) do
      if v.x == x and v.y == y then return true end
    end
  elseif x and type(x) == 'table' then
    return self:isPresent() and self:isVisible(x.x, x.y)
  end
  return false
end

function iVision:visibleThings()
  local result = {}
  for _, v in ipairs(self.visible) do
    local things = GAME.curMap:objectsAt(v.x, v.y)
    for _, t in ipairs(things) do
      if t ~= self then table.insert(result, t) end
    end
  end
  return result
end

return iVision

