local iCombat = {}

function iCombat:attack(target, params)
  params = params or {}
  local atkStat = params.atkStat or "atk"
  local dojStat = params.dojStat or "doj"
  local dmgStat = params.dmgStat or "dmg"
  local soakStat = params.soakStat or "tou"
  local dmgType = params.dmgType or "physical"
  local dmgMult = params.dmgMult or 100
  local edrMult = params.edrMult or 100
  local atk = self:getStat(atkStat)
  local doj = target:getStat(dojStat)
  local hit, over = RNG:oppTest(atk, doj)
  local rawDmg = 0
  local edrDmg = 0 
  local vitDmg = 0 
  local rawEdr = 0
  if hit then
    local soak = target:getStat(soakStat)
    local mod = (dmgMult + over - soak) / 100
    local edrMod = (edrMult + over - soak) / 100
    rawDmg = math.floor(self:getStat(dmgStat) * mod)
    rawEdr = math.floor(self:getStat(dmgStat) * edrMod)
  end  
  if dmgType ~= "magical" then
    local armor = math.floor(target:getTotalEquipped("dfp")/2)
    vitDmg = math.max(rawDmg - armor, 0)
  else
    vitDmg = math.max(rawDmg, 0)
  end
  edrDmg = math.max(rawEdr, 0)
  local finalMsg = {
    "COMBAT", "%s, %s, %s, %d raw, %d vit, %d edr",
    self.eName, target.eName, hit, rawDmg, vitDmg, edrDmg
  }
  debugPrint(finalMsg)
  return hit, vitDmg, edrDmg, dmgType
end

function iCombat:processAttack(target, hitDesc, missDesc, atkParams)
  local hit, dmg, edrDmg, dType = self:attack(target, atkParams)
  local strings = require 'data.strings'
  local hitMsg = strings.combatHitMsg
  local missMsg = strings.combatMissMsg
  if hit then
    GAME:addMsg(hitMsg, self.eName, hitDesc, target.eName, edrDmg, dmg, dType)
    target:takeDmg(dmg, self)
    if target.alive then target:takeEdr(edrDmg, self) end
  else
    GAME:addMsg(missMsg, self.eName, missDesc, target.eName)
  end
  return hit, dmg, edrDmg, dType
end

return iCombat
