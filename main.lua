ROT = require 'vendor.rotLove.src.rot'
SAVE = require 'vendor.binser.binser'
GAME = require 'src.game'
GFX = require 'src.grafix'
BEHOLDER = require 'vendor.beholder.beholder'
MAP = require 'src.map'
ZONE = require 'src.zone'
RNG = require 'src.rng'
GAMESTART = true
saveFile = "test.sav"
saveExists = love.filesystem.exists(saveFile)
require 'src.utils'

local FACTORY = require'src.factory'
local SCREEN = require 'src.screen'
local DIALOG = require 'src.dialog'

local showTT = false
local states = GAME.gameStates

function saveGame() --TODO: clean up saveGame()
  --get RNG state
  GAME.rngState = ROT.RNG:getState()
  local file = love.filesystem.newFile(saveFile)
  file:open("w")
  file:write(SAVE.serialize(GAME))
  file:close()
end

function loadGame()
  if saveExists then
    local file = love.filesystem.newFile(saveFile)
    file:open("r")
    local contents = file:read()
    GAME = SAVE.deserialize(contents)[1]
    for k, v in pairs(GAME.scheduler) do
      debugPrint("GAME", "%s = %s", k, tostring(v))
    end
    if GAME.rngState then ROT.RNG:setState(GAME.rngState) end
    file:close()
    GAME:start()
    return GAME and true
  end
  return false
end

function deleteGame()
  if saveExists then
    love.filesystem.remove(saveFile)
  end
end

function newGame() 
  --TODO: parameters for game options
  --TODO: real new game start
  --load zones from zone files
  debugPrint("NEW", "Creating zones...")
  local fs = love.filesystem.getDirectoryItems("data/zones")
  local tbl
  local conns = {}
  for _, fi in ipairs(fs) do
    local fname = fi:match("(.-).lua")
    tbl = require('data.zones.'..fname)
    local z = ZONE(tbl)
    if tbl.connections then
      table.insert(conns, tbl)
    end
  end
  --connect zones
  for _, t in ipairs(conns) do
    for _, c in ipairs(t.connections) do
      GAME.zones[t.name]:connectZone(c)
    end
  end
  for _, m in pairs(GAME.maps) do
    FACTORY.seed("item", m, m.numItems)
    FACTORY.seed("monster", m, m.numMonsters)
    FACTORY.seed("equipment", m, m.numEquips)
  end
  debugPrint("NEW", "Done loading zones.")

  --set starting zone and map
  GAME.curZone = GAME.zones["Caintigern"]
  GAME.curMap = GAME.maps[GAME.curZone.levels[1]]
  GAME.player = FACTORY.make("player", true, "player", "rogue")
  GAME:refreshSchedule()
  GAME:start()
end

BEHOLDER.observe(print)

function love.load(arg)
  --debug for ZeroBrane
  if arg[#arg] == "-debug" then
    require("mobdebug").start()
    require("mobdebug").off()
  end
  local ft = love.filesystem.getDirectoryItems("listeners")
  for _, v in ipairs(ft) do
    local f = v:match("(.-).lua")
    if f then require("listeners." .. f) end
  end
  love.window.setMode(1024, 786)
  require('src.screen')()
  GAME:switchScreen("Title")
end

function love.update()
  GAME:tick()
  GAME.curScreen:render()
end

function love.draw() 
  GFX.display:draw()
end

function love.keypressed(key, sc)
  local shift = love.keyboard.isScancodeDown('lshift', 'rshift')
  local alt = love.keyboard.isScancodeDown('lalt', 'ralt')
  local ctrl = love.keyboard.isScancodeDown('lctrl', 'rctrl')
  GAME.curScreen:handleInput(sc, shift, alt, ctrl)
end

function love.quit()
  saveGame()
end
